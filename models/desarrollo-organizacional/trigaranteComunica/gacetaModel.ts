import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";

const gacetaModel = dbPostgres.define('gacetaModel',
    {
        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
        },
        idEmpleado:{
          type: DataTypes.INTEGER,
        },
        descripcion: {
            type: new DataTypes.STRING(100)
        },
        fechaLanzamiento: {
            type: new DataTypes.DATE(100)
        },
        fechaRegistro: {
            type: new DataTypes.DATE(100)
        },
        activo: {
            type: DataTypes.TINYINT,
        },
    },
    {
        tableName: "gaceta",
        schema: "recursosHumanos",
        timestamps: false,
    }
    );

export default gacetaModel;
