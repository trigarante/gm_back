import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../../configs/connection";

const ModeloInternoModel = dbPostgres.define('modeloInterno', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    nombre: {
        type: DataTypes.STRING
    },

}, {
    timestamps: false,
    schema: "operaciones",
    tableName: "modeloInterno",
});

export default ModeloInternoModel;
