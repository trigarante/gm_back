import {dbPostgres} from "../../../configs/connection";
import {DataTypes} from "sequelize";

const CotizacionesAliModel = dbPostgres.define('cotizacionesAliModel', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idCotizacion: {
        type: DataTypes.INTEGER,
    },
    idSubRamo: {
        type: DataTypes.INTEGER
    },
    peticion: {
        type: DataTypes.JSON
    },
    respuesta: {
        type: DataTypes.JSON
    },
    fechaCotizacion: {
        type: DataTypes.DATE
    },
}, {
    tableName: "cotizacionesAli",
    schema: "operaciones",
    timestamps: false
})

export default CotizacionesAliModel;
