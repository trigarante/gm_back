import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";


const FormaPagoModel = dbPostgres.define('formaPago', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    descripcion: {
        type: DataTypes.STRING
    },
    activo: {
        type: DataTypes.INTEGER
    },
}, {
    tableName: "formaPago",
    schema: "operaciones",
    timestamps: false
})

export default FormaPagoModel
