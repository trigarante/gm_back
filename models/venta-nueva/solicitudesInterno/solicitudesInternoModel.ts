import {dbPostgres} from "../../../configs/connection";
import {DataTypes} from "sequelize";

const solicitudesInternoModel = dbPostgres.define('solicitudesInterno', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idCotizacionAli: {
        type: DataTypes.INTEGER
    },
    idEmpleado: {
        type: DataTypes.INTEGER
    },
    idSubEtiquetaSolicitud: {
        type: DataTypes.INTEGER
    },
    idEtiquetaSolicitud: {
        type: DataTypes.INTEGER
    },
    idEstadoSolicitud: {
        type: DataTypes.INTEGER
    },
    idDepartamento: {
        type: DataTypes.INTEGER
    },
    comentarios: {
        type: DataTypes.STRING
    },
    fechaSolicitud: {
        type: DataTypes.DATE
    },
    contador: {
        type: DataTypes.INTEGER
    },
    fechaRecontacto: {
        type: DataTypes.DATE
    },
    idIntentoContacto: {
        type: DataTypes.INTEGER
    },
    idFlujoSolicitud: {
        type: DataTypes.INTEGER
    },
    contadorVueltas: {
        type: DataTypes.INTEGER
    },
    emitida: {
        type: DataTypes.INTEGER
    },
}, {
    timestamps: false,
    schema: "operaciones",
    tableName: "solicitudes",
});

export default solicitudesInternoModel;

