import {dbPostgres} from "../../../configs/connection";
import {DataTypes} from "sequelize";

const PaisesModel = dbPostgres.define('paises', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    nombre: {
        type: DataTypes.STRING,
    },
    activo: {
        type: DataTypes.SMALLINT
    },
}, {
    tableName: "paises",
    schema: "operaciones",
    timestamps: false
});
export default PaisesModel;
