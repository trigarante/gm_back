import { DataTypes } from "sequelize";
import {dbPostgres} from "../../../configs/connection";

const TipoDocumentosModel = dbPostgres.define('tipoDocumentos', {
    id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
    },
    idTabla: {
        type: DataTypes.INTEGER,
    },
    nombre: {
        type: DataTypes.STRING
    },
    descripcion: {
        type: DataTypes.STRING
    },
    activo: {
        type: DataTypes.INTEGER
    }
}, {
    tableName: "tipoDocumentos",
    schema: 'operaciones',
    timestamps: false
});

export default TipoDocumentosModel;
