import {dbPostgres} from "../../../configs/connection";
import {DataTypes} from "sequelize";

const ProductosSocioModel = dbPostgres.define('productosSocio', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idSubRamo: {
        type: DataTypes.INTEGER,
    },
    idTipoProducto: {
        type: DataTypes.INTEGER
    },
    nombre: {
        type: DataTypes.STRING
    },
    prioridad: {
        type: DataTypes.SMALLINT
    },
    activo: {
        type: DataTypes.SMALLINT
    },
}, {
    tableName: "productosSocio",
    schema: 'operaciones',
    timestamps: false
});

export default ProductosSocioModel;
