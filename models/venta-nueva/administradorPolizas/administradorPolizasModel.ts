import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";


const AdministradorPolizasModel = dbPostgres.define('administradorPolizas', {
    id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
    },
    idEmpleado: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    idProducto: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    idTipoPago: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    idEstadoPoliza: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    idSocio: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    idProductoSocio: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    idFlujoPoliza: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    idPeriodicidad: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    poliza: {
        type: DataTypes.INTEGER
    },
    fechaInicio: {
        type: DataTypes.DATE
    },
    primaNeta: {
        type: DataTypes.FLOAT
    },
    fechaRegistro: {
        type: DataTypes.DATE
    },
    archivo: {
        type: DataTypes.STRING
    },
    fechaFin: {
        type: DataTypes.DATE
    },
    oficina: {
        type: DataTypes.STRING
    },
    fechaCierre: {
        type: DataTypes.DATE
    },
    fechaCobranza: {
        type: DataTypes.DATE
    },
    idDepartamento: {
        type: DataTypes.INTEGER
    },
}, {
    tableName: "registro",
    schema: "operaciones",
    timestamps: false
})

export default AdministradorPolizasModel
