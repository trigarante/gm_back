import {dbPostgres} from "../../../configs/connection";
import {DataTypes} from "sequelize";

const MotivosDesactivacionModel = dbPostgres.define('motivosDesactivacion', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idTipoMotivo: {
        type: DataTypes.SMALLINT,
    },
    descripcion: {
        type: DataTypes.STRING
    },
    requiereArchivo: {
        type: DataTypes.SMALLINT
    },
    activo: {
        type: DataTypes.SMALLINT
    },
}, {
    tableName: "motivosDesactivacion",
    schema: "generales",
    timestamps: false
});
export default MotivosDesactivacionModel;
