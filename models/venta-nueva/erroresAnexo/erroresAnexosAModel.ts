import {dbPostgres} from "../../../configs/connection";
import {DataTypes} from "sequelize";

const erroresAnexosAModel = dbPostgres.define('erroresAnexosAModel', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idAutorizacionRegistro: {
        type: DataTypes.INTEGER
    },
    idEmpleado: {
        type: DataTypes.INTEGER
    },
    idEstadoCorreccion: {
        type: DataTypes.INTEGER
    },
    idEmpleadoCorreccion: {
        type: DataTypes.INTEGER
    },
    idTipoDocumento: {
        type: DataTypes.INTEGER
    },
    fechaCreacion: {
        type: DataTypes.DATE
    },
    fechaCorreccion: {
        type: DataTypes.DATE
    },
    correcciones: {
        type: DataTypes.JSON
    },
}, {
    timestamps: false,
    schema: "operaciones",
    tableName: "erroresAnexosA",
});

export default erroresAnexosAModel

