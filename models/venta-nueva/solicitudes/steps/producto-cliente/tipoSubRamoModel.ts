import {Sequelize, Model, DataTypes} from "sequelize";
import db from "../../../../../configs/connection";

const sequelize = db;

class TipoSubRamoModel extends Model {
    public id!: number;
    public tipo!: string;
    public activo!: number;
}

TipoSubRamoModel.init({
    id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
    },
    tipo: {
        type: DataTypes.STRING
    },
    activo: {
        type: DataTypes.INTEGER
    },
}, {
    tableName: "tipoSubRamo",
    timestamps: false,
    sequelize,
});

export default TipoSubRamoModel;
