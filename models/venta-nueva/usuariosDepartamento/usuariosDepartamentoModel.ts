import {dbPostgres} from "../../../configs/connection";
import {DataTypes} from "sequelize";

const sequelize = dbPostgres

const usuariosDepartamentoModel = dbPostgres.define('usuariosDepartamentoModel', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idUsuario: {
        type: DataTypes.INTEGER
    },
    idDepartamento: {
        type: DataTypes.INTEGER
    },
    comentarios: {
        type: DataTypes.STRING
    },
}, {
    timestamps: false,
    schema: "generales",
    tableName: "usuariosDepartamento",
});

export default usuariosDepartamentoModel

