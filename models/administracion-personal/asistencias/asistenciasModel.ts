import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";

const AsistenciasModel  = dbPostgres.define('AsistenciasModel',
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        idEmpleado: {
            type: DataTypes.BIGINT,
        },
        idEmpleadoSupervisor: {
            type: DataTypes.BIGINT,
        },
        idEstadoAsistencia: {
            type: DataTypes.INTEGER,
        },
        fechaRegistro: {
            type: DataTypes.DATE,
        },
        solicitud: {
            type: DataTypes.BOOLEAN,
        },
        editable: {
            type: DataTypes.INTEGER,
        }
    },
    {
        tableName: "asistencias",
        timestamps: false,
        schema: "recursosHumanos"
    }
);

export default AsistenciasModel;
