import {dbPostgres} from "../../configs/connection";
import {DataTypes} from "sequelize";

const MotivoJustificacion = dbPostgres.define( 'motivosJustificacion',
    {
        id: {
            type: DataTypes.SMALLINT,
            autoIncrement: true,
            primaryKey: true,
        },
        motivo: {
            type: DataTypes.STRING,
        },
        activo: {
            type: DataTypes.INTEGER,
        },
    },
    {
        tableName: "motivoJustificacion",
        timestamps: false,
        schema: "recursosHumanos"
    }
);

export default MotivoJustificacion;
