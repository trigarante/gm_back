import FormaPagoModel from "../../models/venta-nueva/formaPago/formaPagoModel";


export default class FormaPagoQueries {

    static async findAll(): Promise<any> {
        return await FormaPagoModel.findAll({
            where: {activo: 1}
        })
    }

}



