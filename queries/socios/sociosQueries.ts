import SociosModel from "../../models/venta-nueva/socios/sociosModel";
import {dbPostgres} from "../../configs/connection";
import {QueryTypes} from "sequelize";

export default class SociosQueries {
    static async getByIdPaisStep(idPais) {
        return await SociosModel.findAll({
            attributes: ['id', 'nombreComercial', 'expresionRegular', 'ejemploExpresionRegular'],
            where: {idPais, activo: 1}
        })
    }

    static async getGastosMedigo() {
        return await dbPostgres.query(`
            SELECT
                operaciones."subRamo"."id",
                operaciones.socios."id" AS "idSocio",
                operaciones.socios."nombreComercial",
                operaciones.socios."expresionRegular",
                operaciones.socios."ejemploExpresionRegular"
            FROM
                operaciones."subRamo"
            INNER JOIN
                operaciones.ramo ON operaciones."subRamo"."idRamo" = operaciones.ramo."id"
            INNER JOIN
                operaciones.socios ON operaciones.ramo."idSocio" = operaciones.socios."id"
            WHERE
                operaciones."subRamo".activo = 1
            AND
                operaciones."subRamo"."idTipoSubRamo" = 4
        `, {
            type: QueryTypes.SELECT,
        });
    }
}
