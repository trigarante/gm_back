import ErroresVerificacionModel from "../../models/venta-nueva/erroresVerificacion/ErroresVerificacionModel";

export default class ErroresVerificacionQueries {
    static async getByIdAutorizacionAndDocumento(idVerificacionRegistro, idTipoDocumento) {
        return await ErroresVerificacionModel.findOne({
            where: {
                idVerificacionRegistro,
                idTipoDocumento,
                idEstadoCorreccion:1
            },
        })
    }
    static async update(data, id) {
        return await ErroresVerificacionModel.update(data,{
            where: {id}
        })
    }

    // static async post(data){
    //     return await ErroresAnexosAModel.create(data).then(data => {
    //         return data['id'];
    //     })
    // }
}
