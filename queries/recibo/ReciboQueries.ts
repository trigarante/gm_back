import {QueryTypes} from "sequelize";
import {dbPostgres} from "../../configs/connection";
import RecibosModel from "../../models/venta-nueva/recibo/recibosModel";

export default class ReciboQueries {

    static async findAllByIdRegistroAndActivo(id): Promise<any> {
        return await dbPostgres.query(`
           Select
            re.numero,
            re.cantidad,
            re."fechaVigencia",
            ep.estado,
            r.poliza,
            pa.id AS "idPago",
            "precandidatoPago".nombre AS "nombreEmpleadoPago",
            "precandidatoPago"."apellidoPaterno" AS "apellidoPaternoEmpleadoPago",
            "precandidatoPago"."apellidoMaterno" AS "apellidoMaternoEmpleadoPago",
            re."fechaLiquidacion",
            pa."fechaPago",
            re.id,
            r.id as "idRegistro"
            FROM
            operaciones.recibos re
            INNER JOIN operaciones.registro r ON re."idRegistro" = r.id
            INNER JOIN operaciones."estadoPoliza" ep ON r."idEstadoPoliza" = ep.id
            LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
            LEFT JOIN "recursosHumanos".empleado "empleadoPago" ON pa."idEmpleado" = "empleadoPago"."id"
            LEFT JOIN "recursosHumanos".candidato "candidatoPago" ON "empleadoPago"."idCandidato" = "candidatoPago"."id"
            LEFT JOIN "recursosHumanos".precandidato "precandidatoPago" ON "candidatoPago"."idPrecandidato" = "precandidatoPago"."id"
            Where
            re.activo = 1
            and
            r.id = ${id}
            Order by re.id Asc

        `, {
            type: QueryTypes.SELECT,
        })
    }

    static async post(data) {
        return await RecibosModel.create(data).catch(err => console.log(err))
    }

    static async forPagosByidRegistro(id): Promise<any> {
        return await dbPostgres.query(`
           	Select
                re.id,
                re.cantidad
                FROM
                operaciones.recibos re
                Where
                re.activo = 1 and re.numero = 1
                and
                re."idRegistro" = ${id}

        `, {
            type: QueryTypes.SELECT,
            plain: true
        })
    }
}

// Query antes
// Select
// re.id AS "idRecibo",
//     re."idRegistro",
//     re."idEmpleado",
//     re."idEstadoRecibos",
//     re.numero,
//     re.cantidad,
//     re."fechaVigencia",
//     re."fechaLiquidacion",
//     re."fechaPromesaPago",
//     ere."estadoDescripcion",
//     r."idEstadoPoliza",
//     r."primaNeta",
//     r.poliza,
//     ep.estado,
//     r."idFlujoPoliza",
//     r."idProductoSocio",
//     r."fechaRegistro",
//     r."fechaInicio",
//     so."nombreComercial",
//     pz."idArea",
//     e."idPuesto",
//     se."idEmpresa",
//     preca.nombre,
//     preca."apellidoPaterno",
//     preca."apellidoMaterno",
//     e."idDepartamento",
//     pz."idSede",
//     ar.nombre AS area,
//     pa.datos,
//     pa.id AS "idPago",
//     pa."fechaPago",
//     pa."idFormaPago",
//     pa.archivo,
//     "precandidatoPago".nombre AS "nombreEmpleadoPago",
//     "precandidatoPago"."apellidoPaterno" AS "apellidoPaternoEmpleadoPago",
//     "precandidatoPago"."apellidoMaterno" AS "apellidoMaternoEmpleadoPago",
//     re.activo
// FROM
// operaciones.recibos re
// INNER JOIN operaciones."estadoRecibo" ere ON re."idEstadoRecibos" = ere.id
// INNER JOIN operaciones.registro r ON re."idRegistro" = r.id
// INNER JOIN operaciones."estadoPoliza" ep ON r."idEstadoPoliza" = ep.id
// INNER JOIN "recursosHumanos".empleado e ON re."idEmpleado" = e.id
// INNER JOIN "recursosHumanos".candidato ca ON e."idCandidato" = ca.id
// INNER JOIN "recursosHumanos".precandidato preca ON ca."idPrecandidato" = preca.id
// INNER JOIN "recursosHumanos".departamento depa ON e."idDepartamento" = depa.id
// INNER JOIN "recursosHumanos".plaza pz ON depa."idArea" = pz.id
// INNER JOIN "recursosHumanos".area ar ON pz."idArea" = ar.id
// INNER JOIN "recursosHumanos".sede se ON pz."idSede" = se.id
// INNER JOIN "recursosHumanos".empresas emp ON se."idEmpresa" = emp.id
// INNER JOIN operaciones."productosSocio" ps ON r."idProductoSocio" = ps.id
// INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
// INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
// INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
// LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
// LEFT JOIN "recursosHumanos".empleado "empleadoPago" ON pa."idEmpleado" = "empleadoPago"."id"
// LEFT JOIN "recursosHumanos".candidato "candidatoPago" ON "empleadoPago"."idCandidato" = "candidatoPago"."id"
// LEFT JOIN "recursosHumanos".precandidato "precandidatoPago" ON ca."idPrecandidato" = "precandidatoPago"."id"
// Where
// re.activo = 1
// and
// r.id = ${id}
//     Order by re.id Asc



