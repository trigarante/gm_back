import VerificacionRegistroModel from "../../models/mesaControl/verificacionRegistro/verificacionRegistroModel";
import {dbPostgres} from "../../configs/connection";
import {QueryTypes} from "sequelize";

export default class VerificacionRegistroQueries {

    // PendientesVerificacionView
    static async getAll(fechaInicio, fechaFin,): Promise<any> {
        return await dbPostgres.query(`
           SELECT
                DISTINCT ON (vr."id")
                vr.id,
                vr."idRegistro",
                ev.estado AS "estadoVerificacion",
                vr."verificadoCliente",
                vr."verificadoProducto",
                vr."verificadoRegistro",
                vr."verificadoPago",
                vr."fechaCreacion",
                r.poliza,
                dp.descripcion,
                s."nombreComercial",
                pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" as nombre,
                "precandidatoMesa".nombre || ' ' ||  "precandidatoMesa"."apellidoPaterno" || ' ' || "precandidatoMesa"."apellidoMaterno" AS "nombreMesa",
                cl.nombre || ' ' || cl.paterno || ' ' || cl.materno AS "nombreCliente",
                r.poliza,
                cl.id AS "idCliente",
                r."idDepartamento",
                dp.descripcion,
                pg.id AS idPago,
                pg."fechaPago",
                r."fechaInicio",
                CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
                ep.descripcion AS "estadoPago"
                FROM operaciones."verificacionRegistro" vr
                JOIN operaciones.registro r ON vr."idRegistro" = r.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                LEFT JOIN "recursosHumanos".empleado "empleadoMesa" ON vr."idEmpleado" = "empleadoMesa".id
                LEFT JOIN "recursosHumanos".candidato "candidatoMesa" ON "empleadoMesa"."idCandidato" = "candidatoMesa".id
                LEFT JOIN "recursosHumanos".precandidato "precandidatoMesa" ON "candidatoMesa"."idPrecandidato" = "precandidatoMesa".id
                JOIN operaciones."estadoVerificacion" ev ON vr."idEstadoVerificacion" = ev.id
                JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones.pagos pg ON rb.id = pg."idRecibo"
                JOIN operaciones."estadoPago" ep ON pg."idEstadoPago" = ep.id
                JOIN operaciones."productosSocio" ps ON r."idProductoSocio" = ps."id"
                JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr."id"
                JOIN operaciones.ramo ra ON sr."idRamo" = ra."id"
                JOIN operaciones.socios s ON ra."idSocio" = s.id
                WHERE
                rb.numero =1
                AND
                rb.activo = 1
                AND
                r."idEstadoPoliza" <> 9
                AND
                vr."idEstadoVerificacion" <> 3
                AND  rb."fechaCierre" IS NULL
                AND
                vr."fechaCreacion" BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
                ORDER by vr.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
// PendientesVerificacionView
    static async getAllByDepartamentos(fechaInicio, fechaFin, ...departamentos): Promise<any> {
        return await dbPostgres.query(`
             SELECT
                DISTINCT ON (vr."id")
                vr.id,
                vr."idRegistro",
                ev.estado AS "estadoVerificacion",
                vr."verificadoCliente",
                vr."verificadoProducto",
                vr."verificadoRegistro",
                vr."verificadoPago",
                vr."fechaCreacion",
                r.poliza,
                dp.descripcion,
                s."nombreComercial",
               pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" as nombre,
                "precandidatoMesa".nombre || ' ' ||  "precandidatoMesa"."apellidoPaterno" || ' ' || "precandidatoMesa"."apellidoMaterno" AS "nombreMesa",
                cl.nombre || ' ' || cl.paterno || ' ' || cl.materno AS "nombreCliente",
                r.poliza,
                cl.id AS "idCliente",
                r."idDepartamento",
                dp.descripcion,
                pg.id AS idPago,
                pg."fechaPago",
                r."fechaInicio",
                 CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
                ep.descripcion AS "estadoPago"
                FROM operaciones."verificacionRegistro" vr
                JOIN operaciones.registro r ON vr."idRegistro" = r.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                LEFT JOIN "recursosHumanos".empleado "empleadoMesa" ON vr."idEmpleado" = "empleadoMesa".id
                LEFT JOIN "recursosHumanos".candidato "candidatoMesa" ON "empleadoMesa"."idCandidato" = "candidatoMesa".id
                LEFT JOIN "recursosHumanos".precandidato "precandidatoMesa" ON "candidatoMesa"."idPrecandidato" = "precandidatoMesa".id
                JOIN operaciones."estadoVerificacion" ev ON vr."idEstadoVerificacion" = ev.id
                JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones.pagos pg ON rb.id = pg."idRecibo"
                JOIN operaciones."estadoPago" ep ON pg."idEstadoPago" = ep.id
                JOIN operaciones."productosSocio" ps ON r."idProductoSocio" = ps."id"
                JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr."id"
                JOIN operaciones.ramo ra ON sr."idRamo" = ra."id"
                JOIN operaciones.socios s ON ra."idSocio" = s.id
                WHERE
                rb.numero =1
                AND
                rb.activo = 1
                AND
                r."idEstadoPoliza" <> 9
                AND
                vr."idEstadoVerificacion" <> 3
                AND  rb."fechaCierre" IS NULL
                AND
                vr."fechaCreacion" BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
                AND
                r."idDepartamento" IN (${departamentos})
                ORDER by vr.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    static async getAllByHijos(fechaInicio, fechaFin, ...departamentos): Promise<any> {
        return await dbPostgres.query(`
             SELECT
                DISTINCT ON (vr."id")
                vr.id,
                vr."idRegistro",
                ev.estado AS "estadoVerificacion",
                vr."verificadoCliente",
                vr."verificadoProducto",
                vr."verificadoRegistro",
                vr."verificadoPago",
                vr."fechaCreacion",
                r.poliza,
                dp.descripcion,
                s."nombreComercial",
               pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" as nombre,
                "precandidatoMesa".nombre || ' ' ||  "precandidatoMesa"."apellidoPaterno" || ' ' || "precandidatoMesa"."apellidoMaterno" AS "nombreMesa",
                cl.nombre || ' ' || cl.paterno || ' ' || cl.materno AS "nombreCliente",
                r.poliza,
                cl.id AS "idCliente",
                r."idDepartamento",
                dp.descripcion,
                pg.id AS idPago,
                pg."fechaPago",
                r."fechaInicio",
                CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
                ep.descripcion AS "estadoPago"
                FROM operaciones."verificacionRegistro" vr
                JOIN operaciones.registro r ON vr."idRegistro" = r.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                LEFT JOIN "recursosHumanos".empleado "empleadoMesa" ON vr."idEmpleado" = "empleadoMesa".id
                LEFT JOIN "recursosHumanos".candidato "candidatoMesa" ON "empleadoMesa"."idCandidato" = "candidatoMesa".id
                LEFT JOIN "recursosHumanos".precandidato "precandidatoMesa" ON "candidatoMesa"."idPrecandidato" = "precandidatoMesa".id
                JOIN operaciones."estadoVerificacion" ev ON vr."idEstadoVerificacion" = ev.id
                JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones.pagos pg ON rb.id = pg."idRecibo"
                JOIN operaciones."estadoPago" ep ON pg."idEstadoPago" = ep.id
                JOIN operaciones."productosSocio" ps ON r."idProductoSocio" = ps."id"
                JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr."id"
                JOIN operaciones.ramo ra ON sr."idRamo" = ra."id"
                JOIN operaciones.socios s ON ra."idSocio" = s.id
                WHERE
                rb.numero =1
                AND
                rb.activo = 1
                AND
                r."idEstadoPoliza" <> 9
                AND
                vr."idEstadoVerificacion" <> 3
                AND  rb."fechaCierre" IS NULL
                AND
                vr."fechaCreacion" BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
                AND
                r."idEmpleado" IN (${departamentos})
                ORDER by vr.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    // PendientesVerificacionView
    static async getAllByDepartamentoAndIdEmpleado(idDepartamento,idEmpleado, fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
             SELECT
                DISTINCT ON (vr."id")
                vr.id,
                vr."idRegistro",
                ev.estado AS "estadoVerificacion",
                vr."verificadoCliente",
                vr."verificadoProducto",
                vr."verificadoRegistro",
                vr."verificadoPago",
                vr."fechaCreacion",
                r.poliza,
                dp.descripcion,
                s."nombreComercial",
                r.poliza,
                cl.id AS "idCliente",
                pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" as nombre,
                "precandidatoMesa".nombre || ' ' ||  "precandidatoMesa"."apellidoPaterno" || ' ' || "precandidatoMesa"."apellidoMaterno" AS "nombreMesa",
                cl.nombre || ' ' || cl.paterno || ' ' || cl.materno AS "nombreCliente",
                r."idDepartamento",
                dp.descripcion,
                pg.id AS idPago,
                pg."fechaPago",
                r."fechaInicio",
                CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
                ep.descripcion AS "estadoPago"
                FROM operaciones."verificacionRegistro" vr
                JOIN operaciones.registro r ON vr."idRegistro" = r.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                LEFT JOIN "recursosHumanos".empleado "empleadoMesa" ON vr."idEmpleado" = "empleadoMesa".id
                LEFT JOIN "recursosHumanos".candidato "candidatoMesa" ON "empleadoMesa"."idCandidato" = "candidatoMesa".id
                LEFT JOIN "recursosHumanos".precandidato "precandidatoMesa" ON "candidatoMesa"."idPrecandidato" = "precandidatoMesa".id
                JOIN operaciones."estadoVerificacion" ev ON vr."idEstadoVerificacion" = ev.id
                JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones.pagos pg ON rb.id = pg."idRecibo"
                JOIN operaciones."estadoPago" ep ON pg."idEstadoPago" = ep.id
                JOIN operaciones."productosSocio" ps ON r."idProductoSocio" = ps."id"
                JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr."id"
                JOIN operaciones.ramo ra ON sr."idRamo" = ra."id"
                JOIN operaciones.socios s ON ra."idSocio" = s.id
                WHERE
                rb.numero =1
                AND
                rb.activo = 1
                AND
                r."idEstadoPoliza" <> 9
                AND
                vr."idEstadoVerificacion" <> 3
                AND  rb."fechaCierre" IS NULL
                -- AND
                -- vr."fechaCreacion" BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
                AND
                dp.id = ${idDepartamento}
                AND
                vr."idEmpleado" = ${idEmpleado}
                ORDER by vr.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    // VerificacionRegistroView
    static async getById(id): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                DISTINCT ON (vr."id")
                vr.id AS "idVerificacionRegistro",
                vr."idRegistro",
                vr."idEstadoVerificacion",
                ev.estado AS "estadoVerificacion",
                vr.numero,
                vr."verificadoCliente",
                vr."verificadoProducto",
                vr."verificadoRegistro",
                vr."verificadoPago",
                vr."fechaCreacion",
                pc.id "idProducto",
                vr."verificadoInspeccion",
                vr."idEmpleado",
                s."nombreComercial",
                pr.nombre,
                pr."apellidoPaterno",
                pr."apellidoMaterno",
                "precandidatoMesa".nombre AS "nombreMesa",
                "precandidatoMesa"."apellidoPaterno" AS "apellidoPaternoMesa",
                "precandidatoMesa"."apellidoMaterno" AS "apellidoMaternoMesa",
                r.poliza,
                cl.id AS "idCliente",
                cl.nombre AS "nombreCliente",
                cl.paterno AS "apellidoPaternoCliente",
                cl.materno AS "apellidoMaternoCliente",
                r."idDepartamento",
                dp.descripcion,
                erb."estadoDescripcion" AS "estadoRecibo",
                r."primaNeta",
                r."idEmpleado" AS "idEmpleadoRegistro",
                pg.id AS "idPago",
                pg.archivo AS "archivoPago",
                pg."idEstadoPago",
                pg."fechaPago",
                r."idFlujoPoliza",
                r."fechaInicio",
                tp."cantidadPagos",
                cl."idPais",
                cl.correo,
                cl."telefonoMovil",
                CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
                ep.descripcion AS "estadoPago",
                epol.estado AS "estadoPoliza",
                ins.archivo AS "archivoInspeccion",
                ins.id AS "idInspeccion",
                rb."fechaLiquidacion",
                vr."fechaConclusion",
                sol.id AS "idSolicitud",
                CASE when r.online is not null then 'ONLINE' else 'EN PORTAL' end AS "tipoEmision",
               --CASE WHEN pc.archivo IS NOT NULL THEN pc.archivo ELSE cl.archivo END as "carpetaCliente",
                pc.archivo as "carpetaCliente",
                r.archivo as "carpetaRegistro"
                FROM operaciones."verificacionRegistro" vr
                JOIN operaciones.registro r ON vr."idRegistro" = r.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                JOIN "recursosHumanos".empleado "empleadoMesa" ON vr."idEmpleado" = "empleadoMesa".id
                JOIN "recursosHumanos".candidato "candidatoMesa" ON "empleadoMesa"."idCandidato" = "candidatoMesa".id
                JOIN "recursosHumanos".precandidato "precandidatoMesa" ON "candidatoMesa"."idPrecandidato" = "precandidatoMesa".id
                JOIN operaciones."estadoVerificacion" ev ON vr."idEstadoVerificacion" = ev.id
                JOIN operaciones."tipoPago" tp ON r."idTipoPago" = tp.id
                JOIN operaciones.recibos rb ON r."id" = rb."idRegistro"
                JOIN operaciones."estadoRecibo" erb ON rb."idEstadoRecibos" = erb.id
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones.pagos pg ON rb.id = pg."idRecibo"
                JOIN operaciones."estadoPago" ep ON pg."idEstadoPago" = ep.id
                JOIN operaciones."estadoPoliza" epol ON r."idEstadoPoliza" = epol.id
                JOIN operaciones."productosSocio" ps ON r."idProductoSocio" = ps.id
                JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
                JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
                JOIN operaciones.socios s ON ra."idSocio" = s.id
                LEFT JOIN operaciones.inspecciones ins ON vr."idRegistro" = ins."idRegistro"
                LEFT JOIN operaciones.solicitudes sol ON pc."idSolicitud" = sol.id
                WHERE
                vr.id = ${id}
                ORDER by vr.id DESC
        `, {
            type: QueryTypes.SELECT,
            plain: true
        });
    }
    static async getByIdEstado(id): Promise<any> {
        return await VerificacionRegistroModel.findOne({
            where: {id},
            raw: true
        })
    }
    static async updateEstado(id: number, data): Promise<any> {
        return await VerificacionRegistroModel.update({...data}, {
            where: {
                id,
            }
        });
    }

    static async post(data) {
        return await VerificacionRegistroModel.create(data);
    }

    static async postVerificacionRegistro(data) {
        return await VerificacionRegistroModel.create(data).then(data => {
            return data['idRegistro'];
        }).catch( e => console.log(e))
    }
}
