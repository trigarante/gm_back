import {QueryTypes} from "sequelize";
import {dbPostgres} from "../../configs/connection";
import ClienteModel from "../../models/venta-nueva/cliente/clienteModel";

export default class ClienteQueries {

    static async clienteAll(id): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                c.id,
                c."idPais",
                c.nombre,
                c.paterno,
                c.materno,
                c.cp,
                c.calle,
                c."numInt",
                c."numExt",
                c."idColonia",
                cp.asenta AS colonia,
                c.genero,
                c."telefonoFijo",
                c."telefonoMovil",
                c.correo,
                c."fechaNacimiento",
                c.curp,
                c.rfc,
                c."fechaRegistro",
                c.archivo,
                p.nombre AS "nombrePaises",
                c."razonSocial",
                c."archivoSubido"
            FROM operaciones.cliente c
            LEFT JOIN operaciones.paises p ON c."idPais" = p.id
            LEFT JOIN operaciones."cpSepomex" cp ON c."idColonia" = cp."idCp"
            where c.id = ${id}
        `, {
            type: QueryTypes.SELECT,
            plain: true
        })
    }

    static async getByCurp(curp) {
        return await ClienteModel.findOne({
            where: {curp}
        })
    }

    static async getByRFC(rfc) {
        return await ClienteModel.findOne({
            where: {rfc}
        })
    }

    static async post(data) {
        return await ClienteModel.create(data).then(data => {
            return data['id'];
        }).catch(e => console.log(e))
    }

    static async update(data, id) {
        return await ClienteModel.update(data, {
            where: {id},
        }).catch(err => console.log(err))
    }
}



