import {QueryTypes} from "sequelize";
import gacetaModel from "../../../models/desarrollo-organizacional/trigaranteComunica/gacetaModel";
import {dbPostgres} from "../../../configs/connection";

export default class GacetaQueries {
    static async getGacetas() {
        return await dbPostgres.query(`
        SELECT
            gaceta.id AS id,
            gaceta."idEmpleado" AS "idEmpleado",
            gaceta.descripcion AS descripcion,
            gaceta."fechaLanzamiento" AS "fechaLanzamiento",
            gaceta."fechaRegistro" AS "fechaRegistro",
            gaceta.activo AS activo,
            precandidato.nombre AS nombre,
            precandidato."apellidoPaterno" AS "apellidoPaterno",
            precandidato."apellidoMaterno" AS "apellidoMaterno"
        FROM "recursosHumanos".gaceta
            JOIN "recursosHumanos".empleado ON gaceta."idEmpleado" = empleado.id
            JOIN "recursosHumanos".candidato ON empleado."idCandidato" = candidato.id
            JOIN "recursosHumanos".precandidato ON candidato."idPrecandidato" = precandidato.id
        ORDER BY id DESC
        `,
            {
                type: QueryTypes.SELECT
            })
    }

    static async post(archivos) {
        return await gacetaModel.create(archivos).then(data => {
            return data['id']
        })
    }

    static async getGacetasDelMes() {
        return await dbPostgres.query(`
        SELECT *
        FROM "recursosHumanos".gaceta
        WHERE "fechaLanzamiento" BETWEEN CURRENT_DATE - (EXTRACT (DAY FROM NOW()))::INT
        AND NOW() AND activo = 1
        `,
            {
                type: QueryTypes.SELECT
            })
    }

    static async update(idGaceta: number, data) {
        return await gacetaModel.update({ ...data, activo: 1},
            {
                where: {
                    id: idGaceta,
                }
            })
    }
}
