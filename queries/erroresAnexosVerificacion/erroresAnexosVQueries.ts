import ErroresAnexosVModel from "../../models/venta-nueva/erroresAnexoVerificacion/ErroresAnexosVModel";
export default class ErroresAnexosVQueries {
    static async getByIdAutorizacionAndDocumento(idVerificacionRegistro, idTipoDocumento) {
        return await ErroresAnexosVModel.findOne({
            where: {
                idVerificacionRegistro,
                idTipoDocumento,
            },
        })
    }

    static async update(data, id) {
        return await ErroresAnexosVModel.update(data, {
            where: {id}
        })
    }

    // static async post(data){
    //     return await ErroresAnexosAModel.create(data).then(data => {
    //         return data['id'];
    //     })
    // }
}
