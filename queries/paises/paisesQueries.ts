import PaisesModel from "../../models/venta-nueva/paises/paisesModel";

export default class PaisesQueries {
    static async getActivo() {
        return await PaisesModel.findAll({
            where: {activo: 1},
        })
    }
}
