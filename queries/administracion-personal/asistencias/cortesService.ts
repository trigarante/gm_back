import {dbPostgres} from "../../../configs/connection";
import {QueryTypes} from "sequelize";


export default class CortesService {

    static async traigoCorte(): Promise<any> {
        return await dbPostgres.query(`
            select
                "cortesAdp"."fechaInicial",
                "cortesAdp"."fechaFinal",
                "cortesAdp".id
            from
                "recursosHumanos"."cortesAdp"
            where
                "cortesAdp".activo = 1
                order by id desc
                limit 1
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        });
    }
}
