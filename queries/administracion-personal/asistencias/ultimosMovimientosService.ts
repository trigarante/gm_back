import {QueryTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";
import UltimosMovimientos from "../../../models/administracion-personal/asistencias/ultimosMovimientos";
export default class UltimosMovimientosService {
    static async postMovimiento(data) {
        return await UltimosMovimientos.create(data);
    }

    static async byEmpleadoSupervisor(idEmpleado, idEstadoJustificaciones) {
        const query = idEstadoJustificaciones === 3 ? ' "ultimosMovimientos"."idEstadoJustificacion" = 3' : '"ultimosMovimientos"."idEstadoJustificacion" != 3';
        return (await dbPostgres.query(`
            SELECT
    "ultimosMovimientos"."id",
    "ultimosMovimientos"."idCierre",
    "ultimosMovimientos"."idEmpleado",
    precandidato.nombre || ' ' || precandidato."apellidoPaterno" || ' ' || precandidato."apellidoMaterno" nombre,
    "precandidatoAutorizo".nombre || ' ' || "precandidatoAutorizo"."apellidoPaterno" || ' ' || "precandidatoAutorizo"."apellidoMaterno" "nombreAutorizo",
    "precandidatoADP".nombre || ' ' || "precandidatoADP"."apellidoPaterno" || ' ' || "precandidatoADP"."apellidoMaterno" "nombreADP",
    "ultimosMovimientos"."idEmpleadoSupervisor",
    "ultimosMovimientos"."idEmpleadoADP",
    "ultimosMovimientos".documento,
    "estadoJustificacion".descripcion,
    "ultimosMovimientos"."fechaRegistro",
    "ultimosMovimientos"."fechaInicio",
    "ultimosMovimientos"."fechaFin",
    "ultimosMovimientos"."fechaADP",
    "ultimosMovimientos".comentarios,
    "ultimosMovimientos"."idEstadoJustificacion",
    "ultimosMovimientos"."idEstadoAsistencia",
    "recursosHumanos"."estadoAsistencia".descripcion AS "estadoAsistencia",
    "ultimosMovimientos"."idAsistencia",
    "ultimosMovimientos"."comentariosADP",
    departamento.descripcion departamento,
    area.nombre area
    FROM
    "recursosHumanos"."ultimosMovimientos"
    INNER JOIN "recursosHumanos".empleado ON "recursosHumanos"."ultimosMovimientos"."idEmpleado" = "recursosHumanos".empleado."id"
    INNER JOIN "recursosHumanos".candidato ON "recursosHumanos".empleado."idCandidato" = "recursosHumanos".candidato."id"
    INNER JOIN "recursosHumanos".precandidato ON "recursosHumanos".candidato."idPrecandidato" = "recursosHumanos".precandidato."id"
    LEFT JOIN "recursosHumanos".empleado "empleadoAutorizo" ON "recursosHumanos"."ultimosMovimientos"."idEmpleadoSupervisor" = "empleadoAutorizo"."id"
    LEFT JOIN "recursosHumanos".candidato "candidatoAutorizo" ON "empleadoAutorizo"."idCandidato" = "candidatoAutorizo"."id"
    LEFT JOIN "recursosHumanos".precandidato "precandidatoAutorizo" ON "candidatoAutorizo"."idPrecandidato" = "precandidatoAutorizo"."id"
    LEFT JOIN "recursosHumanos".empleado "empleadoADP" ON "recursosHumanos"."ultimosMovimientos"."idEmpleadoADP" = "empleadoADP"."id"
    LEFT JOIN "recursosHumanos".candidato "candidatoADP" ON "empleadoADP"."idCandidato" = "candidatoADP"."id"
    LEFT JOIN "recursosHumanos".precandidato "precandidatoADP" ON "candidatoADP"."idPrecandidato" = "precandidatoADP"."id"
    INNER JOIN "recursosHumanos"."estadoJustificacion" ON "recursosHumanos"."ultimosMovimientos"."idEstadoJustificacion" = "recursosHumanos"."estadoJustificacion"."id"
    INNER JOIN "recursosHumanos"."estadoAsistencia" ON "recursosHumanos"."ultimosMovimientos"."idEstadoAsistencia" = "recursosHumanos"."estadoAsistencia"."id"
    INNER JOIN "recursosHumanos".plaza ON plaza.id = empleado."idPlaza"
    inner join operaciones.usuarios on empleado.id = usuarios."idEmpleado"
    INNER JOIN "recursosHumanos".departamento  ON departamento.id = usuarios."idDepartamento"
    INNER JOIN "recursosHumanos".area ON area.id = departamento."idArea"
    WHERE
    "idEmpleadoSupervisor" = ${idEmpleado}
    AND
    ${query}
        ORDER BY
    ID DESC
        `,
            {
                type: QueryTypes.SELECT
            }));
    }


}
