import {QueryTypes} from "sequelize";
import {dbPostgres} from "../../configs/connection";
import Empleado from "../../models/administracion-personal/empleados/Empleado";

export default class EmpleadoService {
    static async getEmpleados(estado, aux) {
        return (await dbPostgres.query(`
            SELECT
                empleado.id,
                precandidato.nombre || ' ' || precandidato."apellidoPaterno" || ' ' || precandidato."apellidoMaterno" as "nombreEmpleado",
                empleado."fechaIngreso",
                departamento.descripcion AS departamento,
                puesto.nombre AS puesto,
                sede.nombre as sede,
                empleado."idCandidato",
                precandidato.id as "idPrecandidato",
                area.nombre AS area,
                plaza.id as "idPlaza",
                "turnoEmpleado".turno,
                precandidato.recontratable,
                empleado."fechaBaja",
                precandidato.curp,
                precandidato.calle,
                precandidato."numeroInterior",
                precandidato."numeroExterior",
                precandidato.cp,
                "cpSepomex"."delMun" AS "delegacionMunicipio",
                precandidato.email AS "correoPersonal",
                usuarios.usuario AS "correoEmpresarial",
                precandidato."telefonoMovil",
                precandidato."telefonoFijo",
                candidato.imss,
                candidato."ctaClabe",
                candidato.tarjeta,
                plaza."sueldoDiario",
                plaza."sueldoMensual",
                kpi.cantidad,
                "motivosBaja".descripcion,
                precandidatopadre.nombre || ' ' || precandidatopadre."apellidoPaterno" || ' ' || precandidatopadre."apellidoMaterno" as "nombrePadre"
            FROM "recursosHumanos".plaza
                INNER JOIN "recursosHumanos".puesto ON plaza."idPuesto" = puesto.id
                INNER JOIN "recursosHumanos".departamento ON plaza."idDepartamento" = departamento.id
                INNER JOIN "recursosHumanos".empleado ON plaza.id = empleado."idPlaza"
                INNER JOIN "recursosHumanos".candidato ON empleado."idCandidato" = candidato."id"
                INNER JOIN "recursosHumanos".precandidato ON candidato."idPrecandidato" = precandidato."id"
                LEFT JOIN "operaciones".usuarios ON empleado.id = usuarios."idEmpleado" AND usuarios.estado = 1
                INNER JOIN "recursosHumanos".sede ON sede.id = plaza."idSede"
                INNER JOIN "recursosHumanos".area ON area.id = departamento."idArea"
                INNER JOIN "recursosHumanos"."turnoEmpleado" ON "turnoEmpleado".id = plaza."idTurnoEmpleado"
                LEFT JOIN operaciones."cpSepomex" ON precandidato."idColonia" = "cpSepomex"."idCp"
                LEFT JOIN "recursosHumanos".kpi ON plaza."idKpi" = kpi.id
                LEFT JOIN "recursosHumanos".bajas ON bajas.id =
                    (SELECT
                    "recursosHumanos".bajas."id"
                    FROM
                    "recursosHumanos".precandidato precandidatop
                    INNER JOIN
                    "recursosHumanos".bajas
                    ON
                    precandidatop."id" = "recursosHumanos".bajas."idPrecandidato"
                    INNER JOIN
                    "recursosHumanos"."motivosBaja"
                    ON
                    "recursosHumanos".bajas."idMotivoBaja" = "recursosHumanos"."motivosBaja"."id" AND "motivosBaja"."esBaja" = 1
                    WHERE
                    precandidatop.id = precandidato.id
                    ORDER BY
                    bajas."id" DESC
                    LIMIT 1)
                LEFT JOIN "recursosHumanos"."motivosBaja" ON "motivosBaja".id = bajas."idMotivoBaja"
                INNER JOIN "recursosHumanos".empleado empleadopadre ON plaza."idPadre" = empleadopadre."idPlaza"
                INNER JOIN "recursosHumanos".candidato candidatopadre ON empleadopadre."idCandidato" = candidatopadre."id"
                INNER JOIN "recursosHumanos".precandidato precandidatopadre ON candidatopadre."idPrecandidato" = precandidatopadre."id"
            WHERE
                empleado.activo = ${aux} AND precandidato."idEstadoRH" = ${estado}
                AND precandidato."idEtapa" = 7
            ORDER BY empleado.id DESC
        `, {
            type: QueryTypes.SELECT,
        }));
    }

    static async getDocumentosByIdEmpleado(idEmpleado: number) {
        return (await  dbPostgres.query(`
        SELECT
            empleado.id,
            empleado.documentos
        FROM
            "recursosHumanos".empleado
        WHERE
            empleado.id = ${idEmpleado}
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        }));
    }

    static async getPlazasHijo(idPlaza: number) {
        return (await dbPostgres.query(`
            SELECT
                empleado."id",
                empleado."idPlaza",
                precandidato.nombre || ' ' || precandidato."apellidoPaterno" || ' ' || precandidato."apellidoMaterno" AS nombre,
                "solicitudVacaciones"."id" "idVacacion",
                "solicitudVacaciones"."idEstadoVacacion",
                "turnoEmpleado".turno turno
            FROM
                "recursosHumanos".empleado
                INNER JOIN "recursosHumanos".plaza ON plaza."id" = empleado."idPlaza"
                INNER JOIN "recursosHumanos".candidato ON empleado."idCandidato" = candidato."id"
                INNER JOIN "recursosHumanos".precandidato ON candidato."idPrecandidato" = precandidato."id"
                LEFT JOIN "recursosHumanos"."solicitudVacaciones" ON "solicitudVacaciones"."idEmpleado" = empleado."id"
                AND date("solicitudVacaciones"."fechaInicio") = date(now()) and "solicitudVacaciones"."idEstadoVacacion"=2
                inner join "recursosHumanos"."turnoEmpleado" on "turnoEmpleado".id = plaza."idTurnoEmpleado"
            WHERE
                plaza."idPadre" = ${idPlaza} AND precandidato."idEstadoRH" = 1 and empleado.activo = 1
                ORDER BY empleado.id ASC`, {
            type: QueryTypes.SELECT,
        }));
    }
    static async create(data: Empleado) {
        return await Empleado.create({...data}).then(data => {
            return data.id
        });
    }

    static async getAll() {
        return await Empleado.findAll();
    }

    static async updateEmpleado(idEmpleado: number, data: any) {
        return await Empleado.update({ ...data }, {
            where: {
                id: idEmpleado
            }
        });
    }

    static async getIdPlazaByIdCandidato(idCandidato: number) {
        return await dbPostgres.query(`
        SELECT
            plaza.id as "idPlaza",
            precandidato.id as "idPrecandidato",
            "prospectoRRHH".id as "idProspecto"
        FROM
            "recursosHumanos".plaza
            INNER JOIN "recursosHumanos"."prospectoRRHH" ON "prospectoRRHH"."idPlaza" = plaza.id
            INNER JOIN "recursosHumanos".precandidato ON precandidato."idProspectoRRHH" = "prospectoRRHH".id
            INNER JOIN "recursosHumanos".candidato ON candidato."idPrecandidato" = precandidato.id
            WHERE candidato.id = ${idCandidato}
        `,{
            type: QueryTypes.SELECT,
            plain: true,
        })
    }

    static async getEmpleadoByIdPlaza(idPlaza: number) {
        return await dbPostgres.query(`
        SELECT
            empleado.id,
            empleado."idPlaza"
        FROM "recursosHumanos".empleado
            INNER JOIN "recursosHumanos".plaza ON empleado."idPlaza" = plaza.id
			INNER JOIN "recursosHumanos".candidato ON empleado."idCandidato" = candidato.id
			INNER JOIN "recursosHumanos".precandidato ON candidato."idPrecandidato" = precandidato.id
        WHERE
			precandidato."idEstadoRH" = 1
			AND empleado.activo = 1
            AND plaza.id = ${idPlaza}
        `,{
            type: QueryTypes.SELECT,
            plain: true,
        })
    }
    static async getForJustificaciones(idArea: number) {
        return await dbPostgres.query(`
        SELECT
        empleado.id,
        precandidato.nombre || ' ' || precandidato."apellidoPaterno" || ' ' || precandidato."apellidoMaterno" as "nombreEmpleado"
        FROM "recursosHumanos".plaza
        INNER JOIN "recursosHumanos".empleado ON plaza.id = empleado."idPlaza"
        INNER JOIN "recursosHumanos".candidato ON empleado."idCandidato" = candidato."id"
        INNER JOIN "recursosHumanos".precandidato ON candidato."idPrecandidato" = precandidato."id"
        INNER JOIN "recursosHumanos".area ON area.id = plaza."idArea"
        WHERE
        empleado.activo = 1 AND precandidato."idEstadoRH" = 1
        AND precandidato."idEtapa" = 7 AND area.id = ${idArea}
        ORDER BY empleado.id DESC
        `,{
            type: QueryTypes.SELECT,
        })
    }

    // static async enviarAEmpleado(id: number, data) {
    //     const candidatoData: Candidato =  await Candidato.findOne({
    //         where: {id},
    //     });
    //
    //     const precandidatoData: Precandidato = await Precandidato.findOne({
    //         where: {
    //             id: candidatoData.idPrecandidato
    //         }
    //     });
    //
    //     return await Precandidato.update({ ...precandidatoData, ...data }, {
    //         where: {
    //             id: precandidatoData.id
    //         }
    //     });
    // }

    static async getCorreoPersonal(idEmpleado){
        return await dbPostgres.query(`
            SELECT p.email
            FROM "recursosHumanos".empleado e
                JOIN "recursosHumanos".candidato c on e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato p on c."idPrecandidato" = p.id
            WHERE
            e.id = ${idEmpleado}
            `,{
            type: QueryTypes.SELECT,
            plain: true
        })

    }



    static async getIds(idEmpleado: number) {
        return await dbPostgres.query(`
        SELECT
            e.id,
            c.id AS "idCandidato",
            p.id AS "idPrecandidato",
            p."idProspectoRRHH" as "idProspecto",
            c."idBanco",
            c."ctaClabe",
            c.tarjeta,
            c.imss,
            c.rfc,
            e."fechaIngreso",
            CONCAT(p.nombre, ' ',p."apellidoPaterno", ' ',p."apellidoMaterno") as nombre,
            CONCAT (a.codigo, s.codigo, dep.codigo) AS cc,
            tp.nombre as puesto,
            tpl.nombre AS "tipoPlaza",
            e.documentos,
            em.nombre as "empresaEmpleadora",
            emp.nombre as "empresaPagadora",
            dep.descripcion AS departamento,
            CONCAT(te.turno, ' - ',te.horario) as turno,
            e."cantidadVacaciones",
            pl."sueldoMensual",
            a.nombre AS area,
            a.id as "idArea",
            e."tarjetaSecundaria",
            pl.kpi
        FROM
            "recursosHumanos".empleado e
        INNER JOIN "recursosHumanos".candidato c ON c.id = e."idCandidato"
        INNER JOIN "recursosHumanos".precandidato p ON p.id = c."idPrecandidato"
        INNER JOIN "recursosHumanos".plaza pl ON pl.id = e."idPlaza"
        INNER JOIN "recursosHumanos".puesto tp ON  tp.id = pl."idPuesto"
        INNER JOIN "recursosHumanos"."tipoPlaza" tpl ON tpl.id = pl."idTipoPlaza"
        INNER JOIN "recursosHumanos".empresas em ON em.id = pl."idEmpresaEmpleadora"
        LEFT JOIN "recursosHumanos".empresas emp ON emp.id = pl."idEmpresaPagadora"
        INNER JOIN "recursosHumanos".departamento dep ON dep.id = pl."idDepartamento"
        INNER JOIN "recursosHumanos"."turnoEmpleado" te ON te.id = pl."idTurnoEmpleado"
        INNER JOIN "recursosHumanos".area a ON pl."idArea" = a.id
        INNER JOIN "recursosHumanos".segmento s ON dep."idSegmento" = s.id

        WHERE e.id = ${idEmpleado}`, {
            type: QueryTypes.SELECT,
            plain: true,
        })
    }

    static async getInfoForToken(idEmpleado: number) {
        return await dbPostgres.query(`
        SELECT e.id, p.id as "idPlaza", d.id as "idDepartamento"
        FROM "recursosHumanos".empleado e
        INNER JOIN "recursosHumanos".plaza p ON p.ID = e."idPlaza"
        INNER JOIN "recursosHumanos".departamento d ON d.id = p."idDepartamento"
        WHERE e.id = ${idEmpleado}
        `, {
            type: QueryTypes.SELECT,
            plain: true
        })
    }

    static async getById(id){
        return await Empleado.findOne({
            where: {id},
            raw: true
        })
    }

    static async getByIdCandidato(idCandidato){
        return await Empleado.findOne({
            where: {idCandidato, activo: 1},
            raw: true
        })
    }

    static async getPermisosVisualizacion(idEmpleado: number): Promise<any> {
        return await dbPostgres.query(`
            SELECT gu."idPermisosVisualizacion" as "idPermisoVisualizacion",
            pl."idDepartamento",
            u.id AS "idUsuario",
            pl."idPuesto",
            pl.id
            FROM operaciones.usuarios u
            INNER JOIN generales."grupoUsuarios" gu ON gu.id = u."idGrupo"
            INNER JOIN "recursosHumanos".empleado e ON e.id = u."idEmpleado"
            INNER JOIN "recursosHumanos".plaza pl ON pl.id = e."idPlaza"
            WHERE e.id = ${idEmpleado}
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        });
    }

    static async getDepartamentoByIdUsuario(idUsuario){
        return await dbPostgres.query(`
        SELECT ud."idDepartamento"
        FROM generales."usuariosDepartamento" ud
        WHERE ud."idUsuario" = ${idUsuario}`,
            {
                type: QueryTypes.SELECT
            })
    }

    static async getActivos(){
        return await dbPostgres.query(`
           SELECT
            "recursosHumanos".empleado."id",
            "recursosHumanos".precandidato.nombre || ' ' || "recursosHumanos".precandidato."apellidoPaterno" || ' ' || "recursosHumanos".precandidato."apellidoMaterno" AS nombre,
            "recursosHumanos".empleado."fechaIngreso",
            "recursosHumanos".sede.nombre AS sede,
            "recursosHumanos".departamento.descripcion,
            "recursosHumanos".puesto.nombre AS puesto,
            MAX ( "carruselEmpleados".contador ) AS contador,
            CASE

            WHEN MAX ( "carruselEmpleados".permiso ) = 1 THEN
            'ACTIVO' ELSE'INACTIVO'
            END permiso,
            "solicitudesCarrusel".ID AS "idSolicitudesCarrusel"
        FROM
            "recursosHumanos".empleado
            INNER JOIN "recursosHumanos".candidato ON "recursosHumanos".empleado."idCandidato" = "recursosHumanos".candidato."id"
            INNER JOIN "recursosHumanos".precandidato ON "recursosHumanos".candidato."idPrecandidato" = "recursosHumanos".precandidato."id"
            INNER JOIN "recursosHumanos".plaza ON "recursosHumanos".empleado."idPlaza" = "recursosHumanos".plaza."id"
            INNER JOIN "recursosHumanos".departamento ON "recursosHumanos".plaza."idDepartamento" = "recursosHumanos".departamento."id"
            INNER JOIN "recursosHumanos".puesto ON "recursosHumanos".plaza."idPuesto" = "recursosHumanos".puesto."id"
            INNER JOIN "recursosHumanos"."carruselEmpleados" ON "recursosHumanos".empleado."id" = "recursosHumanos"."carruselEmpleados"."idEmpleado"
            INNER JOIN "recursosHumanos".sede ON "recursosHumanos".plaza."idSede" = "recursosHumanos".sede."id"
            LEFT JOIN operaciones."solicitudesCarrusel" ON "recursosHumanos".empleado."id" = operaciones."solicitudesCarrusel"."idEmpleado"
            AND "solicitudesCarrusel"."idEstadoSolicitudCarrusel" < 3
            INNER JOIN operaciones.usuarios ON usuarios."idEmpleado" = empleado.ID
        WHERE
            usuarios.usuario IS NOT NULL
            AND precandidato."idEstadoRH" = 1
        GROUP BY
            empleado."id",
            "recursosHumanos".precandidato.nombre || ' ' || "recursosHumanos".precandidato."apellidoPaterno" || ' ' || "recursosHumanos".precandidato."apellidoMaterno",
            sede.nombre,
            departamento.descripcion,
            puesto.nombre,
            "solicitudesCarrusel"."id"
        ORDER BY
            empleado."id" ASC
            `,{
            type: QueryTypes.SELECT,
        })

    }
    static async empleadosByDepto(idDepto: number) {
        return await dbPostgres.query(`
            SELECT
                plaza.id,
                plaza."puestoEspecifico",
                "turnoEmpleado".turno
            FROM
                "recursosHumanos".plaza
                INNER JOIN "recursosHumanos".departamento ON "recursosHumanos".plaza."idDepartamento" = "recursosHumanos".departamento."id"
                INNER JOIN "recursosHumanos".area ON "recursosHumanos".departamento."idArea" = "recursosHumanos".area."id"
                LEFT JOIN "recursosHumanos"."turnoEmpleado" ON plaza."idTurnoEmpleado" = "turnoEmpleado".id
            WHERE
                plaza."idEstadoPlaza" = 4
                 AND plaza."idDepartamento" = ${idDepto}
                 `, {
            type: QueryTypes.SELECT,
        })
    }

    static async getJefeDirecto(idDepto: number) {
        return await dbPostgres.query(`
            SELECT
                plaza.id,
                plaza."puestoEspecifico",
                precandidato.nombre || ' ' || precandidato."apellidoPaterno" || ' ' || precandidato."apellidoMaterno" AS nombre
            FROM
                "recursosHumanos".plaza
                INNER JOIN "recursosHumanos".departamento ON "recursosHumanos".plaza."idDepartamento" = "recursosHumanos".departamento."id"
                INNER JOIN "recursosHumanos".area ON "recursosHumanos".departamento."idArea" = "recursosHumanos".area."id"
                LEFT JOIN "recursosHumanos"."turnoEmpleado" ON plaza."idTurnoEmpleado" = "turnoEmpleado".id
                LEFT JOIN "recursosHumanos".empleado ON plaza.id = empleado."idPlaza" AND empleado.activo = 1
                INNER JOIN "recursosHumanos".candidato ON empleado."idCandidato" = candidato."id"
                INNER JOIN "recursosHumanos".precandidato ON candidato."idPrecandidato" = precandidato."id"
            WHERE
                plaza."idPuesto" NOT IN (8,12,39,19)
                AND plaza."idDepartamento" = ${idDepto}
                 `, {
            type: QueryTypes.SELECT,
        })
    }

    static async getPlazaHijoByPadre(idPadre: number) {
        return await dbPostgres.query(`
            SELECT
                plaza.id,
                plaza."puestoEspecifico",
                "turnoEmpleado".turno
            FROM
                "recursosHumanos".plaza
                INNER JOIN "recursosHumanos".departamento ON "recursosHumanos".plaza."idDepartamento" = "recursosHumanos".departamento."id"
                INNER JOIN "recursosHumanos".area ON "recursosHumanos".departamento."idArea" = "recursosHumanos".area."id"
                LEFT JOIN "recursosHumanos"."turnoEmpleado" ON plaza."idTurnoEmpleado" = "turnoEmpleado".id
            WHERE
                plaza."idEstadoPlaza" = 4
                 AND plaza."idPadre" = ${idPadre}
                 `, {
            type: QueryTypes.SELECT,
        })
    }

    static async getTarjeta(tarjetaSecundaria){
        return await Empleado.findOne({
            where: {tarjetaSecundaria},
            raw: true
        })
    }
}
