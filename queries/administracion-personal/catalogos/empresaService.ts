import EmpresaModel from "../../../models/administracion-personal/catalogos/empresaModel";

export default class EmpresaService {
    static async get() {
        return await EmpresaModel.findAll({
            where: {activo: 1}
        });
    }
}
