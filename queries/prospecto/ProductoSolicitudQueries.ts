import ProductoSolicitudModel from "../../models/venta-nueva/prospecto/productoSolicitudModel";
import {Op, QueryTypes} from "sequelize";
import {dbPostgres} from "../../configs/connection";

export default class ProductoSolicitudQueries {
    static async getByIdProspecto(idProspecto) {
        return await ProductoSolicitudModel.findAll({
            where: {idProspecto, datos:{[Op.not]: null}}
        })
    }

    static async getById(id) {
        return await dbPostgres.query(`
        SELECT
            DISTINCT ON (operaciones."productoSolicitud"."id")
            operaciones."productoSolicitud"."id",
            operaciones."productoSolicitud".datos,
            operaciones.prospecto.numero,
            operaciones.prospecto.correo,
            operaciones.prospecto.nombre,
            operaciones.prospecto.sexo,
            operaciones.prospecto.edad,
            operaciones."productoSolicitud".datos::json ->>'nombre',
            CASE WHEN operaciones."cotizacionesAli".peticion::json ->>'socio' is null THEN operaciones."cotizacionesAli".peticion::json ->>'aseguradora' ELSE operaciones."cotizacionesAli".peticion::json ->>'socio' END AS aseguradora
        FROM
            operaciones."productoSolicitud"
        INNER JOIN
            operaciones.prospecto
        ON
            operaciones."productoSolicitud"."idProspecto" = operaciones.prospecto."id"
        INNER JOIN operaciones.cotizaciones ON operaciones.cotizaciones."idProducto" = operaciones."productoSolicitud"."id"
        INNER JOIN operaciones."cotizacionesAli" ON operaciones.cotizaciones."id" = operaciones."cotizacionesAli"."idCotizacion"
        WHERE
            "productoSolicitud".id = ${id}
        ORDER BY operaciones."productoSolicitud"."id" DESC
        `, {
            type: QueryTypes.SELECT,
            plain: true
        })
    }

    static async post(data) {
        return await ProductoSolicitudModel.create(data).then(data => {
            return data.id;
        })
    }

    static async postGM(data) {
        return await ProductoSolicitudModel.create(data).then(data => {
            return data;
        }).catch(e => console.log(e))
    }
}
