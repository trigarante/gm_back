import {QueryTypes} from "sequelize";
import {dbPostgres} from "../../configs/connection";

export default class AdministradorPolizasQueries {

    static async getPermisosVisualizacion(idEmpleado: number): Promise<any> {
        return await dbPostgres.query(`
            SELECT gu."idPermisosVisualizacion" as "idPermisoVisualizacion",
            pl."idDepartamento",
            u.id AS "idUsuario",
            pl."idPuesto",
            pl.id
            FROM operaciones.usuarios u
            INNER JOIN generales."grupoUsuarios" gu ON gu.id = u."idGrupo"
            INNER JOIN "recursosHumanos".empleado e ON e.id = u."idEmpleado"
            INNER JOIN "recursosHumanos".plaza pl ON pl.id = e."idPlaza"
            WHERE e.id = ${idEmpleado}
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        });
    }
    static async getAll(fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            r.id,
            r.poliza,
            r."fechaInicio",
            r."primaNeta",
            r."fechaRegistro",
            r.archivo,
            tp."cantidadPagos",
            tp."tipoPago",
            ps.nombre AS "productoSocio",
            r."idEstadoPoliza",
            ep.estado AS "estadoPoliza",
            preca.nombre,
            preca."apellidoPaterno",
            preca."apellidoMaterno",
            pz."idDepartamento",
            pc.datos,
            CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
            preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" as "nombreEmpleado",
            cl.nombre || ' ' || cl.paterno || ' ' || cl.materno AS "nombreCliente",
            so."nombreComercial",
            dp.descripcion,
            ep.estado AS "estadoPoliza",
            er."estadoDescripcion" AS "estadoRecibo",
            case when pa.id is not null then espa.descripcion else 'SIN PAGO' end AS "estadoPago",
            cl."telefonoMovil",
            r."fechaInicio",
            CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
            case when re."fechaCierre" is not null then 1 else 0 end AS cierre,
            re.id AS "idRecibo",
            --CASE WHEN pc.archivo IS NOT NULL THEN pc.archivo ELSE cl.archivo END as "carpetaCliente",
            pc.archivo as "carpetaCliente",
            re."idEstadoRecibos",
            pa.id AS "idPago",
            pa."idEstadoPago",
            pc."idSolicitud",
            r.emitida,
            cl.id as "idCliente"
            FROM
            operaciones.registro r
            INNER JOIN operaciones."productosSocio" ps On r."idProductoSocio" = ps.id
            INNER JOIN operaciones."productoCliente" pc On r."idProducto" = pc.id
            INNER JOIN operaciones."estadoPoliza" ep On r."idEstadoPoliza" = ep.id
            INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
            INNER JOIN "recursosHumanos".plaza pz ON e."idPlaza" = pz.id
            INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
            INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
            INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" =preca.id
            INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
            INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
            INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
            INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
            INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
            INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
            LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
            LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
            LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
            where
            re.activo = 1
            and
            r."idFlujoPoliza" = 26
            and
            re.numero = 1
            and
            r."idEstadoPoliza" <> 9
            --and
            --(
                --pa.id is not null
                    --or
                        --(pa.id is null
                        --and
                    --extract (day from (now() -r."fechaInicio")) < 15
                --)
            --)
            and r."fechaRegistro" BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
            ORDER BY
                r.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    static async getAllByDepartamentos(departamentos, fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
                SELECT
            r.id,
            r.poliza,
            r."fechaInicio",
            r."primaNeta",
            r."fechaRegistro",
            r.archivo,
            tp."cantidadPagos",
            tp."tipoPago",
            ps.nombre AS "productoSocio",
            r."idEstadoPoliza",
            ep.estado AS "estadoPoliza",
            preca.nombre,
            preca."apellidoPaterno",
            preca."apellidoMaterno",
            pz."idDepartamento",
            pc.datos,
            CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
            preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" as "nombreEmpleado",
            cl.nombre || ' ' || cl.paterno || ' ' || cl.materno AS "nombreCliente",
            so."nombreComercial",
            dp.descripcion,
            ep.estado AS "estadoPoliza",
            er."estadoDescripcion" AS "estadoRecibo",
            case when pa.id is not null then espa.descripcion else 'SIN PAGO' end AS "estadoPago",
            cl."telefonoMovil",
            r."fechaInicio",
            CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
            case when re."fechaCierre" is not null then 1 else 0 end AS cierre,
            re.id AS "idRecibo",
            --CASE WHEN pc.archivo IS NOT NULL THEN pc.archivo ELSE cl.archivo END as "carpetaCliente",
            pc.archivo as "carpetaCliente",
            re."idEstadoRecibos",
            pa.id AS "idPago",
            pa."idEstadoPago",
            pc."idSolicitud",
            r.emitida,
            cl.id as "idCliente"
            FROM
            operaciones.registro r
            INNER JOIN operaciones."productosSocio" ps On r."idProductoSocio" = ps.id
            INNER JOIN operaciones."productoCliente" pc On r."idProducto" = pc.id
            INNER JOIN operaciones."estadoPoliza" ep On r."idEstadoPoliza" = ep.id
            INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
            INNER JOIN "recursosHumanos".plaza pz ON e."idPlaza" = pz.id
            INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
            INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
            INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" =preca.id
            INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
            INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
            INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
            INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
            INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
            INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
            LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
            LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
            LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
            where
            re.activo = 1
            and
            r."idFlujoPoliza" = 26
            and
            re.numero = 1
            and
            r."idEstadoPoliza" <> 9
            --and
            --(
                --pa.id is not null
                    --or
                        --(pa.id is null
                        --and
                    --extract (day from (now() -r."fechaInicio")) < 15
                --)
            --)
            and r."idDepartamento" IN (${departamentos})
            and r."fechaRegistro" BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
            ORDER BY r.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    static async getAllByHijos(departamentos, fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            r.id,
            r.poliza,
            r."fechaInicio",
            r."primaNeta",
            r."fechaRegistro",
            r.archivo,
            tp."cantidadPagos",
            tp."tipoPago",
            ps.nombre AS "productoSocio",
            r."idEstadoPoliza",
            ep.estado AS "estadoPoliza",
            preca.nombre,
            preca."apellidoPaterno",
            preca."apellidoMaterno",
            pz."idDepartamento",
            pc.datos,
            CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
            preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" as "nombreEmpleado",
            cl.nombre || ' ' || cl.paterno || ' ' || cl.materno AS "nombreCliente",
            so."nombreComercial",
            dp.descripcion,
            ep.estado AS "estadoPoliza",
            er."estadoDescripcion" AS "estadoRecibo",
            case when pa.id is not null then espa.descripcion else 'SIN PAGO' end AS "estadoPago",
            cl."telefonoMovil",
            r."fechaInicio",
            CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
            case when re."fechaCierre" is not null then 1 else 0 end AS cierre,
            re.id AS "idRecibo",
            --CASE WHEN pc.archivo IS NOT NULL THEN pc.archivo ELSE cl.archivo END as "carpetaCliente",
            pc.archivo as "carpetaCliente",
            re."idEstadoRecibos",
            pa.id AS "idPago",
            pa."idEstadoPago",
            pc."idSolicitud",
            r.emitida,
            cl.id as "idCliente"
            FROM
            operaciones.registro r
            INNER JOIN operaciones."productosSocio" ps On r."idProductoSocio" = ps.id
            INNER JOIN operaciones."productoCliente" pc On r."idProducto" = pc.id
            INNER JOIN operaciones."estadoPoliza" ep On r."idEstadoPoliza" = ep.id
            INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
            INNER JOIN "recursosHumanos".plaza pz ON e."idPlaza" = pz.id
            INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
            INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
            INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" =preca.id
            INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
            INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
            INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
            INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
            INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
            INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
            LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
            LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
            LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
            where
            re.activo = 1
            and
            r."idFlujoPoliza" = 26
            and
            re.numero = 1
            and
            r."idEstadoPoliza" <> 9
            --and
            --(
                --pa.id is not null
                    --or
                        --(pa.id is null
                        --and
                    --extract (day from (now() -r."fechaInicio")) < 15
                --)
            --)
            and r."idEmpleado" IN (${departamentos})
            and r."fechaRegistro" BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
            ORDER BY r.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    static async getAllByDepartamentoAndIdEmpleado(idDepartamento, idEmpleado, fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            r.id,
            r.poliza,
            r."fechaInicio",
            r."primaNeta",
            r."fechaRegistro",
            r.archivo,
            tp."cantidadPagos",
            tp."tipoPago",
            ps.nombre AS "productoSocio",
            r."idEstadoPoliza",
            ep.estado AS "estadoPoliza",
            preca.nombre,
            preca."apellidoPaterno",
            preca."apellidoMaterno",
            pz."idDepartamento",
            pc.datos,
            CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
            preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" as "nombreEmpleado",
            cl.nombre || ' ' || cl.paterno || ' ' || cl.materno AS "nombreCliente",
            so."nombreComercial",
            dp.descripcion,
            ep.estado AS "estadoPoliza",
            er."estadoDescripcion" AS "estadoRecibo",
            case when pa.id is not null then espa.descripcion else 'SIN PAGO' end AS "estadoPago",
            cl."telefonoMovil",
            r."fechaInicio",
            CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
            case when re."fechaCierre" is not null then 1 else 0 end AS cierre,
            re.id AS "idRecibo",
            --CASE WHEN pc.archivo IS NOT NULL THEN pc.archivo ELSE cl.archivo END as "carpetaCliente",
            pc.archivo as "carpetaCliente",
            re."idEstadoRecibos",
            pa.id AS "idPago",
            pa."idEstadoPago",
            pc."idSolicitud",
            r.emitida,
            cl.id as "idCliente"
            FROM
            operaciones.registro r
            INNER JOIN operaciones."productosSocio" ps On r."idProductoSocio" = ps.id
            INNER JOIN operaciones."productoCliente" pc On r."idProducto" = pc.id
            INNER JOIN operaciones."estadoPoliza" ep On r."idEstadoPoliza" = ep.id
            INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
            INNER JOIN "recursosHumanos".plaza pz ON e."idPlaza" = pz.id
            INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
            INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
            INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" =preca.id
            INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
            INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
            INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
            INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
            INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
            INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
            LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
            LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
            LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
            where
            re.activo = 1
            and
            r."idFlujoPoliza" = 26
            and
            re.numero = 1
            and
            r."idEstadoPoliza" <> 9
            --and
            --(
                --pa.id is not null
                    --or
                        --(pa.id is null
                        --and
                    --extract (day from (now() -r."fechaInicio")) < 15
                --)
            --)
            -- and r."idDepartamento" = ${idDepartamento}
            and r."idEmpleado" = ${idEmpleado}
            and r."fechaRegistro" BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
            ORDER BY r.id DESC
        `, {
            type: QueryTypes.SELECT,
        })
    }
}
