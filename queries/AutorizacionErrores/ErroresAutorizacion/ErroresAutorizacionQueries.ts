import ErroresAutorizacionModel
    from "../../../models/venta-nueva/autorizacionErrores/erroresAutorizacion/ErroresAutorizacionModel";

export default class ErroresAutorizacionQueries {
    static async getByIdAutorizacionAndDocumento(idAutorizacionRegistro, idTipoDocumento) {
        return await ErroresAutorizacionModel.findOne({
            where: {
                idAutorizacionRegistro,
                idTipoDocumento,
                idEstadoCorreccion:1
            },
        })
    }

    static async post(data) {
        return await ErroresAutorizacionModel.create(data).then(data => {
            return data['id'];
        })
    }

    static async update(data, id) {
        return await ErroresAutorizacionModel.update(data, {
            where: {id},
        })
    }
}
