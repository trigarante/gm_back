import {QueryTypes} from "sequelize";
import {dbPostgres} from "../../configs/connection";

export default class PlazaQueries {
    static async getPlazasHijo(id) {
        return await dbPostgres.query(`
        SELECT empleado.id
        FROM "recursosHumanos".empleado
        JOIN "recursosHumanos".plaza ON plaza.id = empleado."idPlaza"
        WHERE plaza."idPadre" = ${id}`, {
            type: QueryTypes.SELECT
        })
    }
}
