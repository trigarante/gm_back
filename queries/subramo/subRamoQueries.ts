import {dbPostgres} from "../../configs/connection";
import {QueryTypes} from "sequelize";

export default class SubRamoQueries {
    static async getByIdRamo(idRamo) {
        return await  dbPostgres.query(`
        SELECT operaciones."subRamo".id, operaciones."subRamo"."idRamo", operaciones."tipoSubRamo".tipo AS "tipoSubRamo" FROM operaciones."subRamo"
        INNER JOIN operaciones."tipoSubRamo" ON operaciones."tipoSubRamo".id = operaciones."subRamo"."idTipoSubRamo"
        WHERE operaciones."subRamo"."idRamo" = '${idRamo}' AND operaciones."subRamo".activo =1`, {
            type: QueryTypes.SELECT,
        })
    }
}
