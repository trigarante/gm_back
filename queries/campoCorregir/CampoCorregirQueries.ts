import CampoCorregirModel from "../../models/venta-nueva/campoCorregir/CampoCorregirModel";
import {Op} from "sequelize";

export default class CampoCorregirQueries {
    static async getByIdTablaAndActivos(idTabla) {
        return await CampoCorregirModel.findAll({
            where: {
                activo: 1,
                idTabla,
                id: {
                    [Op.notIn]: [5, 6, 13, 16, 29]
                }
            },
        })
    }

    static async getFisico(idTabla) {
        return await CampoCorregirModel.findAll({
            where: {
                activo: 1,
                    idTabla,
                },
        })
    }
}
