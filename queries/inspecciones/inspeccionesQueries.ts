import InspeccionesModel from "../../models/venta-nueva/inspecciones/inspeccionesModel";

export default class InspeccionesQueries {
    static async post(data) {
        return await InspeccionesModel.create(data)
    }
}
