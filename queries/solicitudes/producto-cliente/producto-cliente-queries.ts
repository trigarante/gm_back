import {QueryTypes} from "sequelize";
import ProductoClienteModel from "../../../models/venta-nueva/solicitudes/steps/producto-cliente/productoClienteModel";
import db, {dbPostgres} from "../../../configs/connection";

export default class ProductoClienteQueries {
    static async findNoSerie(noSerie): Promise<any> {
        return await dbPostgres.query(`
        SELECT
            operaciones."flujoPoliza".estado as "noSerie"
        FROM
            operaciones."productoCliente"
        INNER JOIN
            operaciones.registro
            ON
            operaciones."productoCliente"."id" = operaciones.registro."idProducto"
        INNER JOIN
            operaciones."flujoPoliza"
            ON
            operaciones.registro."idFlujoPoliza" = operaciones."flujoPoliza"."id"
        WHERE "productoCliente"."noSerie" = '${noSerie}'
            AND (registro."idEstadoPoliza" != 3 OR registro."idEstadoPoliza" = 5)
        LIMIT 1
        `, {
            type: QueryTypes.SELECT,
            plain: true
        })
    }

    static async post(data) {
        return await ProductoClienteModel.create(data).then(data => {
            return data['id'];
        }).catch(err => console.log(err))
    }

    static async update(data, id) {
        return await ProductoClienteModel.update(data, {
            where: {id}
        })
    }

    static async findProductoClienteById(idProductoCliente): Promise<any> {
        return await db.query(`
            SELECT
                productoCliente.id AS id,
                productoCliente.idCliente AS idCliente,
                productoCliente.idSolictud AS idSolictud,
                productoCliente.datos AS datos,
                productoCliente.idEmision AS idEmision,
                cliente.nombre AS nombre,
                cliente.paterno AS paterno,
                cliente.materno AS materno,
                subRamo.descripcion AS descripcion,
                productoCliente.idSubRamo AS idSubRamo,
                cliente.archivo AS archivo,
                cliente.curp AS curp,
                cliente.correo AS correo,
                cliente.telefonoMovil AS telefonoMovil
            FROM
                productoCliente
             JOIN cliente ON productoCliente.idCliente = cliente.id
             JOIN subRamo ON productoCliente.idSubRamo = subRamo.id
             WHERE
                productoCliente.id = ${idProductoCliente}
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }

    static async updateProductoCliente(idProductoCliente: number, data: any): Promise<any> {
        return await ProductoClienteModel.update({...data}, {
            where: {
                id: idProductoCliente,
            }
        });
    }

    static async getByIdSolicitud(idSolicitud) {
        return await ProductoClienteModel.findOne({
            raw: true,
            where: {idSolicitud}
        })
    }

    static async percaPago(id) {
        return dbPostgres.query(`
        SELECT
        registro.id,
        solicitudes.id as "IdSolicitud",
        "productoCliente".id as "idProducto",
        "productoCliente"."idCliente",
        cliente.correo,
        cliente.nombre,
        cliente."telefonoMovil" AS numero,
        CASE WHEN prospecto."idUsuarioApp" IS NULL THEN 0 ELSE prospecto."idUsuarioApp" END  AS "idUsuarioApp"
        FROM operaciones.registro
        JOIN operaciones."productoCliente" ON "productoCliente".id = registro."idProducto"
        JOIN operaciones.cliente ON cliente.id = "productoCliente"."idCliente"
        JOIN operaciones.solicitudes ON solicitudes.id = "productoCliente"."idSolicitud"
        JOIN operaciones."cotizacionesAli" ON "cotizacionesAli".id = solicitudes."idCotizacionAli"
        JOIN operaciones.cotizaciones ON cotizaciones.id = "cotizacionesAli"."idCotizacion"
        JOIN operaciones."productoSolicitud" ON "productoSolicitud".id = cotizaciones."idProducto"
        JOIN operaciones.prospecto ON prospecto.id = "productoSolicitud"."idProspecto"
        WHERE "productoCliente"."idSolicitud" = ${id}
        `, {
            type: QueryTypes.SELECT,
            plain: true
        })
    }

    static async getForInspecciones(id) {
        return dbPostgres.query(`
        SELECT
        pc.id,
        pc.datos::json ->> 'numeroPlacas' as "numeroPlacas"
        FROM operaciones."productoCliente" pc
        INNER JOIN operaciones.registro r ON r."idProducto" = pc.id
        WHERE r.id = ${id}
        `, {
            type: QueryTypes.SELECT,
            plain: true
        })
    }

    static async getById(id) {
        return await ProductoClienteModel.findOne({
            where: {id}
        })
    }

}
