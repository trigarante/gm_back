
import {QueryTypes} from "sequelize";
import ClienteModel from "../../../models/venta-nueva/solicitudes/steps/crear-cliente/cliente-model";
import db, {dbPostgres} from "../../../configs/connection";

export default class CrearClienteQueries {
    static async getByIdSinJson(idSolicitud: number): Promise<any> {
        const fechaActual = new Date();
        const fechaMx = fechaActual.toLocaleDateString('es-MX', {
            timeZone: 'America/Mexico_City'
        });
        const fecha = new Date(fechaMx);
        const mesNum = fecha.getDate();
        const setMonth = fecha.setDate(mesNum - 1);
        return await db.query(`
            SELECT
                solicitudes.id AS id,
                solicitudes.idCotizacionAli AS idCotizacionAli,
                solicitudes.idEmpleado AS idEmpleado,
                solicitudes.idEstadoSolicitud AS idEstadoSolicitud,
                solicitudes.fechaSolicitud AS fechaSolicitud,
                solicitudes.comentarios AS comentarios,
                cotizacionesAliSnJson.idCotizacion AS idCotizacion,
                cotizacionesAliSnJson.fechaCotizacion AS fechaCotizacion,
                cotizacionesAliSnJson.fechaActualizacion AS fechaActualizacion,
                estadoSolicitud.estado AS estado,
                estadoSolicitud.activo AS activo,
                empleado.idUsuario AS idUsuario,
                empleado.idBanco AS idBanco,
                empleado.idCandidato AS idCandidato,
                empleado.idPuesto AS idPuesto,
                empleado.idTipoPuesto AS idTipoPuesto,
                empleado.puestoDetalle AS puestoDetalle,
                empleado.idSubarea AS idSubarea,
                subarea.subarea AS subArea,
                candidato.idPrecandidato AS idPrecandidato,
                precandidato.nombre AS nombre,
                precandidato.apellidoPaterno AS apellidoPaterno,
                precandidato.apellidoMaterno AS apellidoMaterno,
                cotizaciones.idProducto AS idProducto,
                cotizaciones.idTipoContacto AS idTipoContacto,
                productoSolicitud.idProspecto AS idProspecto,
                cotizacionesAliSnJson.idSubRamo AS idSubRamo,
            IF
            ((
                json_unquote(
                json_extract( productoSolicitud.datos,` + ` '$.nombre' )) IS NULL
                ),
                prospecto.nombre,
            CONVERT ( json_unquote( json_extract( productoSolicitud.datos, '$.nombre' )) USING utf8 )) AS nombreProspecto,
            prospecto.correo AS correoProspecto,
            IF
                ((
                    json_unquote(
                    json_extract( productoSolicitud.datos, '$.telefono' )) IS NULL
                ),
                prospecto.numero,
                    json_unquote(
                    json_extract( productoSolicitud.datos, '$.telefono' ))) AS numeroProspecto,
                    solicitudes.idEtiquetaSolicitud AS idEtiquetaSolicitud,
                    solicitudes.idFlujoSolicitud AS idFlujoSolicitud,
                    etiquetaSolicitud.descripcion AS etiquetaSolicitud,
                    etiquetaSolicitud.activo AS etiquetaSolicitudActivo
            FROM
                solicitudes
                JOIN cotizacionesAliSnJson ON solicitudes.idCotizacionAli = cotizacionesAliSnJson.id
                JOIN empleado ON solicitudes.idEmpleado = empleado.id
                JOIN estadoSolicitud ON solicitudes.idEstadoSolicitud = estadoSolicitud.id
                JOIN candidato ON empleado.idCandidato = candidato.id
                JOIN precandidato ON candidato.idPrecandidato = precandidato.id
                JOIN cotizaciones ON cotizacionesAliSnJson.idCotizacion = cotizaciones.id
                JOIN productoSolicitud ON cotizaciones.idProducto = productoSolicitud.id
                JOIN prospecto ON productoSolicitud.idProspecto = prospecto.id
                JOIN etiquetaSolicitud ON solicitudes.idEtiquetaSolicitud = etiquetaSolicitud.id
                JOIN subarea ON solicitudes.idSubarea = subarea.id
            WHERE
                 ${new Date(setMonth).toLocaleDateString()} < solicitudes.fechaSolicitud AND solicitudes.id = ${idSolicitud}
            ORDER BY
                solicitudes.id DESC
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }

    static async findByCurp(curp: string): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                *
            FROM
                operaciones.cliente
            WHERE
                 operaciones.cliente.curp = '${curp}'
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }

    static async findByrfc(rfc: string): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                *
            FROM
                operaciones.cliente
            WHERE
                 operaciones.cliente.rfc = '${rfc}'
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }

    static async getPaises(): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                *
            FROM
                operaciones.paises
        `, {
            type: QueryTypes.SELECT,
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }

    static async getColoniaByCp(cp: any): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                *
            FROM
                operaciones."cpSepomex"
            WHERE
                operaciones."cpSepomex".cp = ${cp}
        `, {
            type: QueryTypes.SELECT,
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }

    static async getCLienteById(idCliente: number): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                operaciones.cliente.id AS id,
                operaciones.cliente."idPais" AS idPais,
                operaciones.cliente.nombre AS nombre,
                operaciones.cliente.paterno AS paterno,
                operaciones.cliente.materno AS materno,
                operaciones.cliente.cp AS cp,
                operaciones.cliente.calle AS calle,
                operaciones.cliente."numInt" AS numInt,
                operaciones.cliente."numExt" AS numExt,
                operaciones.cliente."idColonia" AS idColonia,
                operaciones."cpSepomex".asenta AS colonia,
                operaciones.cliente.genero AS genero,
                operaciones.cliente."telefonoFijo" AS telefonoFijo,
                operaciones.cliente."telefonoMovil" AS telefonoMovil,
                operaciones.cliente.correo AS correo,
                operaciones.cliente.curp AS curp,
                operaciones.cliente.rfc AS rfc,
                operaciones.cliente."fechaRegistro" AS fechaRegistro,
                operaciones.cliente.archivo AS archivo,
                operaciones.paises.nombre AS nombrePaises,
                operaciones.cliente."razonSocial" AS razonSocial,
                operaciones.cliente."archivoSubido" AS archivoSubido
            FROM
                operaciones.cliente
            JOIN operaciones.paises ON operaciones.cliente."idPais" = operaciones.paises.id
            LEFT JOIN operaciones."cpSepomex" ON operaciones."cpSepomex"."idCp" = operaciones.cliente."idColonia"
            WHERE
                operaciones.cliente.id = ${idCliente}
        `, {
            type: QueryTypes.SELECT,
            plain: true
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }

    static async updateCliente(idCliente: number, data: ClienteModel): Promise<any> {
        return await ClienteModel.update({...data}, {
            where: {
                id: idCliente,
            }
        });
    }
}
