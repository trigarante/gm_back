import {dbPostgres} from "../../configs/connection";
import {QueryTypes} from "sequelize";
import solicitudesInternoModel from "../../models/venta-nueva/solicitudesInterno/solicitudesInternoModel";

export default class SolicitudesInternoQueries {
    static async getSolicitudes(idEmpleado): Promise<any> {
        return await dbPostgres.query(`
            SELECT *
                FROM operaciones.solicitudes
                WHERE operaciones.solicitudes."idEmpleado" = ${idEmpleado}
                    ORDER BY
                    id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    static async getAll(fechaInicio, fechaFin, idTipoContacto): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                operaciones.solicitudes.id,
                operaciones.solicitudes."idCotizacionAli",
                operaciones.solicitudes."idEmpleado",
                operaciones.solicitudes."idEstadoSolicitud",
                operaciones.solicitudes."fechaSolicitud",
                operaciones.solicitudes.comentarios,
                "operaciones"."productoSolicitud".id "idProducto",
                operaciones.solicitudes."idEtiquetaSolicitud",
                operaciones.solicitudes."idFlujoSolicitud",
                "operaciones"."cotizacionesAli"."idCotizacion",
                "operaciones"."cotizacionesAli"."fechaCotizacion",
                "operaciones"."cotizacionesAli"."fechaActualizacion",
                "operaciones"."cotizacionesAli"."idSubRamo",
                operaciones."estadoSolicitud".estado,
                operaciones."estadoSolicitud".activo,
                "recursosHumanos".departamento.id "idDepartamento",
                "recursosHumanos".departamento.descripcion "descripcionDepartamento",
                "recursosHumanos".empleado."idCandidato",
                "recursosHumanos".precandidato.nombre || ' ' || "recursosHumanos".precandidato."apellidoPaterno" || ' ' || "recursosHumanos".precandidato."apellidoMaterno" "nombreEmpleado",
                "operaciones"."prospecto".id "idProspecto",
                "operaciones"."prospecto".nombre "nombreCliente",
                "operaciones"."etiquetaSolicitud".descripcion "etiquetaSolicitud",
                "operaciones"."etiquetaSolicitud".activo "etiquetaSolicitudActivo",
                "operaciones"."cotizaciones"."idTipoContacto",
                "operaciones"."prospecto".numero
            FROM operaciones.solicitudes
                INNER JOIN "operaciones"."cotizacionesAli" ON "operaciones"."cotizacionesAli"."id" = operaciones.solicitudes."idCotizacionAli"
                INNER JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = operaciones."solicitudes"."idEmpleado"
                INNER JOIN operaciones."estadoSolicitud" ON operaciones.solicitudes."idEstadoSolicitud" = operaciones."estadoSolicitud".id
                INNER JOIN "recursosHumanos".candidato ON "recursosHumanos".candidato.id = "recursosHumanos"."empleado"."idCandidato"
                INNER JOIN "recursosHumanos".precandidato ON "recursosHumanos".precandidato.id = "recursosHumanos"."candidato"."idPrecandidato"
                INNER JOIN "operaciones"."cotizaciones" ON "operaciones"."cotizaciones"."id" = "operaciones"."cotizacionesAli"."idCotizacion"
                INNER JOIN "operaciones"."productoSolicitud" ON "operaciones"."productoSolicitud"."id" = "operaciones"."cotizaciones"."idProducto"
                INNER JOIN "operaciones"."prospecto" ON "operaciones"."prospecto"."id" = "operaciones"."productoSolicitud"."idProspecto"
                INNER JOIN "operaciones"."etiquetaSolicitud" ON "operaciones"."etiquetaSolicitud"."id" = operaciones.solicitudes."idEtiquetaSolicitud"
                INNER JOIN "recursosHumanos".plaza ON "recursosHumanos".plaza.id = "recursosHumanos"."empleado"."idPlaza"
                INNER JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento.id = "recursosHumanos"."plaza"."idDepartamento"
            WHERE
                operaciones.solicitudes."fechaSolicitud" BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
            AND
                solicitudes.emitida = 0
            AND
                cotizaciones."idTipoContacto" IN (${idTipoContacto}, 19)
            AND
                operaciones.solicitudes."idDepartamento" IN (138, 58, 134, 135, 136, 137, 139)
            ORDER BY
                operaciones.solicitudes.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    // AND
    // cotizaciones."idTipoContacto" IN (${idTipoContacto}, 19) and solicitudes."idEstadoSolicitud" IN (${idEstadoSolicitud}, ${idEstadoSolicitudGM})

    static async getAllByDepartamentos(fechaInicio, fechaFin,idTipoContacto, ...departamentos): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                operaciones.solicitudes.id,
                operaciones.solicitudes."idCotizacionAli",
                operaciones.solicitudes."idEmpleado",
                operaciones.solicitudes."idEstadoSolicitud",
                operaciones.solicitudes."fechaSolicitud",
                operaciones.solicitudes.comentarios,
                "operaciones"."productoSolicitud".id "idProducto",
                operaciones.solicitudes."idEtiquetaSolicitud",
                operaciones.solicitudes."idFlujoSolicitud",
                "operaciones"."cotizacionesAli"."idCotizacion",
                "operaciones"."cotizacionesAli"."fechaCotizacion",
                "operaciones"."cotizacionesAli"."fechaActualizacion",
                "operaciones"."cotizacionesAli"."idSubRamo",
                operaciones."estadoSolicitud".estado,
                operaciones."estadoSolicitud".activo,
                "recursosHumanos".departamento.id "idDepartamento",
                "recursosHumanos".departamento.descripcion "descripcionDepartamento",
                "recursosHumanos".empleado."idCandidato",
                "recursosHumanos".precandidato.nombre || ' ' || "recursosHumanos".precandidato."apellidoPaterno" || ' ' || "recursosHumanos".precandidato."apellidoMaterno" "nombreEmpleado",
                "operaciones"."prospecto".id "idProspecto",
                "operaciones"."prospecto".nombre "nombreCliente",
                "operaciones"."etiquetaSolicitud".descripcion "etiquetaSolicitud",
                "operaciones"."etiquetaSolicitud".activo "etiquetaSolicitudActivo",
                "operaciones"."cotizaciones"."idTipoContacto",
                "operaciones"."prospecto".numero
            FROM operaciones.solicitudes
                INNER JOIN "operaciones"."cotizacionesAli" ON "operaciones"."cotizacionesAli"."id" = operaciones.solicitudes."idCotizacionAli"
                INNER JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = operaciones."solicitudes"."idEmpleado"
                INNER JOIN operaciones."estadoSolicitud" ON operaciones.solicitudes."idEstadoSolicitud" = operaciones."estadoSolicitud".id
                INNER JOIN "recursosHumanos".candidato ON "recursosHumanos".candidato.id = "recursosHumanos"."empleado"."idCandidato"
                INNER JOIN "recursosHumanos".precandidato ON "recursosHumanos".precandidato.id = "recursosHumanos"."candidato"."idPrecandidato"
                INNER JOIN "operaciones"."cotizaciones" ON "operaciones"."cotizaciones"."id" = "operaciones"."cotizacionesAli"."idCotizacion"
                INNER JOIN "operaciones"."productoSolicitud" ON "operaciones"."productoSolicitud"."id" = "operaciones"."cotizaciones"."idProducto"
                INNER JOIN "operaciones"."prospecto" ON "operaciones"."prospecto"."id" = "operaciones"."productoSolicitud"."idProspecto"
                INNER JOIN "operaciones"."etiquetaSolicitud" ON "operaciones"."etiquetaSolicitud"."id" = operaciones.solicitudes."idEtiquetaSolicitud"
                INNER JOIN "recursosHumanos".plaza ON "recursosHumanos".plaza.id = "recursosHumanos"."empleado"."idPlaza"
                INNER JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento.id = "recursosHumanos"."plaza"."idDepartamento"
            WHERE operaciones.solicitudes."fechaSolicitud" BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
            AND
                solicitudes.emitida = 0
            AND
                operaciones.solicitudes."idDepartamento" IN (182,183,215,138, 58, 134, 135, 136, 137, 139)
            AND
            cotizaciones."idTipoContacto"IN (${idTipoContacto}, 19)
            ORDER BY operaciones.solicitudes.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    // and solicitudes."idEstadoSolicitud" IN (${idEstadoSolicitud}, ${idEstadoSolicitudGM})
    static async getAllByHijos(fechaInicio, fechaFin, idTipoContacto, idEstadoSolicitud, idEstadoSolicitudGM,...departamentos): Promise<any> {
        console.log('waaaaaaaaaa');
        return await dbPostgres.query(`
            SELECT
                operaciones.solicitudes.id,
                operaciones.solicitudes."idCotizacionAli",
                operaciones.solicitudes."idEmpleado",
                operaciones.solicitudes."idEstadoSolicitud",
                operaciones.solicitudes."fechaSolicitud",
                operaciones.solicitudes.comentarios,
                "operaciones"."productoSolicitud".id "idProducto",
                operaciones.solicitudes."idEtiquetaSolicitud",
                operaciones.solicitudes."idFlujoSolicitud",
                "operaciones"."cotizacionesAli"."idCotizacion",
                "operaciones"."cotizacionesAli"."fechaCotizacion",
                "operaciones"."cotizacionesAli"."fechaActualizacion",
                "operaciones"."cotizacionesAli"."idSubRamo",
                operaciones."estadoSolicitud".estado,
                operaciones."estadoSolicitud".activo,
                "recursosHumanos".departamento.id "idDepartamento",
                "recursosHumanos".departamento.descripcion "descripcionDepartamento",
                "recursosHumanos".empleado."idCandidato",
                "recursosHumanos".precandidato.nombre || ' ' || "recursosHumanos".precandidato."apellidoPaterno" || ' ' || "recursosHumanos".precandidato."apellidoMaterno" "nombreEmpleado",
                "operaciones"."prospecto".id "idProspecto",
                "operaciones"."prospecto".nombre "nombreCliente",
                "operaciones"."etiquetaSolicitud".descripcion "etiquetaSolicitud",
                "operaciones"."etiquetaSolicitud".activo "etiquetaSolicitudActivo",
                "operaciones"."cotizaciones"."idTipoContacto",
                "operaciones"."prospecto".numero
            FROM operaciones.solicitudes
                INNER JOIN "operaciones"."cotizacionesAli" ON "operaciones"."cotizacionesAli"."id" = operaciones.solicitudes."idCotizacionAli"
                INNER JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = operaciones."solicitudes"."idEmpleado"
                INNER JOIN operaciones."estadoSolicitud" ON operaciones.solicitudes."idEstadoSolicitud" = operaciones."estadoSolicitud".id
                INNER JOIN "recursosHumanos".candidato ON "recursosHumanos".candidato.id = "recursosHumanos"."empleado"."idCandidato"
                INNER JOIN "recursosHumanos".precandidato ON "recursosHumanos".precandidato.id = "recursosHumanos"."candidato"."idPrecandidato"
                INNER JOIN "operaciones"."cotizaciones" ON "operaciones"."cotizaciones"."id" = "operaciones"."cotizacionesAli"."idCotizacion"
                INNER JOIN "operaciones"."productoSolicitud" ON "operaciones"."productoSolicitud"."id" = "operaciones"."cotizaciones"."idProducto"
                INNER JOIN "operaciones"."prospecto" ON "operaciones"."prospecto"."id" = "operaciones"."productoSolicitud"."idProspecto"
                INNER JOIN "operaciones"."etiquetaSolicitud" ON "operaciones"."etiquetaSolicitud"."id" = operaciones.solicitudes."idEtiquetaSolicitud"
                INNER JOIN "recursosHumanos".plaza ON "recursosHumanos".plaza.id = "recursosHumanos"."empleado"."idPlaza"
                INNER JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento.id = "recursosHumanos"."plaza"."idDepartamento"
            WHERE operaciones.solicitudes."fechaSolicitud" BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
            AND
                solicitudes.emitida = 0
            AND
                operaciones.solicitudes."idEmpleado" IN (138, 58, 134, 135, 136, 137, 139)
            AND
            cotizaciones."idTipoContacto" IN (19) and solicitudes."idEstadoSolicitud" IN (${idEstadoSolicitud}, ${idEstadoSolicitudGM})
            ORDER BY operaciones.solicitudes.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    static async getAllByDepartamentoAndIdEmpleado(idDepartamento, idEmpleado, fechaInicio, fechaFin, idTipoContacto): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                operaciones.solicitudes.id,
                operaciones.solicitudes."idCotizacionAli",
                operaciones.solicitudes."idEmpleado",
                operaciones.solicitudes."idEstadoSolicitud",
                operaciones.solicitudes."fechaSolicitud",
                operaciones.solicitudes.comentarios,
                "operaciones"."productoSolicitud".id "idProducto",
                operaciones.solicitudes."idEtiquetaSolicitud",
                operaciones.solicitudes."idFlujoSolicitud",
                "operaciones"."cotizacionesAli"."idCotizacion",
                "operaciones"."cotizacionesAli"."fechaCotizacion",
                "operaciones"."cotizacionesAli"."fechaActualizacion",
                "operaciones"."cotizacionesAli"."idSubRamo",
                operaciones."estadoSolicitud".estado,
                operaciones."estadoSolicitud".activo,
                "recursosHumanos".departamento.id "idDepartamento",
                "recursosHumanos".departamento.descripcion "descripcionDepartamento",
                "recursosHumanos".empleado."idCandidato",
                "recursosHumanos".precandidato.nombre || ' ' || "recursosHumanos".precandidato."apellidoPaterno" || ' ' || "recursosHumanos".precandidato."apellidoMaterno" "nombreEmpleado",
                "operaciones"."prospecto".id "idProspecto",
                "operaciones"."prospecto".nombre "nombreCliente",
                "operaciones"."etiquetaSolicitud".descripcion "etiquetaSolicitud",
                "operaciones"."etiquetaSolicitud".activo "etiquetaSolicitudActivo",
                "operaciones"."cotizaciones"."idTipoContacto",
                "operaciones"."prospecto".numero
            FROM operaciones.solicitudes
                INNER JOIN "operaciones"."cotizacionesAli" ON "operaciones"."cotizacionesAli"."id" = operaciones.solicitudes."idCotizacionAli"
                INNER JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = operaciones."solicitudes"."idEmpleado"
                INNER JOIN operaciones."estadoSolicitud" ON operaciones.solicitudes."idEstadoSolicitud" = operaciones."estadoSolicitud".id
                INNER JOIN "recursosHumanos".candidato ON "recursosHumanos".candidato.id = "recursosHumanos"."empleado"."idCandidato"
                INNER JOIN "recursosHumanos".precandidato ON "recursosHumanos".precandidato.id = "recursosHumanos"."candidato"."idPrecandidato"
                INNER JOIN "operaciones"."cotizaciones" ON "operaciones"."cotizaciones"."id" = "operaciones"."cotizacionesAli"."idCotizacion"
                INNER JOIN "operaciones"."productoSolicitud" ON "operaciones"."productoSolicitud"."id" = "operaciones"."cotizaciones"."idProducto"
                INNER JOIN "operaciones"."prospecto" ON "operaciones"."prospecto"."id" = "operaciones"."productoSolicitud"."idProspecto"
                INNER JOIN "operaciones"."etiquetaSolicitud" ON "operaciones"."etiquetaSolicitud"."id" = operaciones.solicitudes."idEtiquetaSolicitud"
                INNER JOIN "recursosHumanos".plaza ON "recursosHumanos".plaza.id = "recursosHumanos"."empleado"."idPlaza"
                INNER JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento.id = "recursosHumanos"."plaza"."idDepartamento"
            WHERE operaciones.solicitudes."fechaSolicitud" BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
            AND
            solicitudes.emitida = 0
            AND
            "recursosHumanos".departamento.id = ${idDepartamento}
            AND
            operaciones.solicitudes."idEmpleado" = ${idEmpleado}
            AND
            cotizaciones."idTipoContacto"IN (${idTipoContacto}, 19)
            ORDER BY operaciones.solicitudes.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    // and solicitudes."idEstadoSolicitud" IN (${idEstadoSolicitud}, ${idEstadoSolicitudGM})
    // AND solicitudes."idEstadoSolicitud" IN (${idEstadoSolicitud}, ${idEstadoSolicitudGM})

    static async getById(idSolicitud): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                operaciones.solicitudes.id,
                operaciones.solicitudes."idCotizacionAli",
                operaciones.solicitudes."idEmpleado",
                operaciones.solicitudes."fechaSolicitud",
                "recursosHumanos".departamento.id "idDepartamento",
                "recursosHumanos".departamento.descripcion "descripcionDepartemento",
                "operaciones"."cliente".nombre "nombreCliente",
                "operaciones"."cliente"."paterno" "apellidoPaternoCliente",
                "operaciones"."cliente"."materno" "apellidoMaternoCliente",
                "operaciones"."productoCliente".id "idProducto",
                "operaciones"."prospecto".id "idProspecto",
                "recursosHumanos".precandidato.nombre "nombreEmpleado",
                "recursosHumanos".precandidato."apellidoPaterno" "apellidoPaternoEmpleado",
                "recursosHumanos".precandidato."apellidoMaterno" "apellidoMaternoEmpleado",
                "operaciones"."prospecto".numero,
                "operaciones"."cotizacionesAli".peticion,
                "operaciones"."cotizacionesAli".respuesta
            FROM operaciones.solicitudes
                LEFT JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = operaciones."solicitudes"."idEmpleado"
                LEFT JOIN "recursosHumanos".candidato ON "recursosHumanos".candidato.id = "recursosHumanos"."empleado"."idCandidato"
                LEFT JOIN "recursosHumanos".precandidato ON "recursosHumanos".precandidato.id = "recursosHumanos"."candidato"."idPrecandidato"
                LEFT JOIN "recursosHumanos".plaza ON "recursosHumanos".plaza.id = "recursosHumanos"."empleado"."idPlaza"
                LEFT JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento.id = "recursosHumanos"."plaza"."idDepartamento"
                LEFT JOIN "operaciones"."productoCliente" ON "operaciones"."productoCliente"."idSolicitud" = operaciones.solicitudes.id
                LEFT JOIN "operaciones"."cliente" ON "operaciones"."cliente"."id" = "operaciones"."productoCliente"."idCliente"
                LEFT JOIN "operaciones"."cotizacionesAli" ON "operaciones"."cotizacionesAli"."id" = operaciones.solicitudes."idCotizacionAli"
                INNER JOIN "operaciones"."cotizaciones" ON "operaciones"."cotizaciones"."id" = "operaciones"."cotizacionesAli"."idCotizacion"
                INNER JOIN "operaciones"."productoSolicitud" ON "operaciones"."productoSolicitud"."id" = "operaciones"."cotizaciones"."idProducto"
                INNER JOIN "operaciones"."prospecto" ON "operaciones"."prospecto"."id" = "operaciones"."productoSolicitud"."idProspecto"
            WHERE operaciones.solicitudes.id = ${idSolicitud}
            ORDER BY operaciones.solicitudes.id DESC
        `, {
            type: QueryTypes.SELECT,
            plain: true
        });
    }

    static async getStepsById(idSolicitud): Promise<any> {
        return await dbPostgres.query(`
SELECT
                DISTINCT ON (registro."id")
                solicitudes."id",
                "cotizacionesAli".peticion,
                "cotizacionesAli".respuesta,
                "productoCliente"."idCliente",
                "productoCliente"."id" AS "idProducto",
                registro.id as "idRegistro",
                registro.id AS "idRecibo",
                pagos.id AS "idPago",
                "cotizacionesAli"."idSubRamo"
            FROM
                operaciones.solicitudes
            INNER JOIN
                operaciones."cotizacionesAli"
                ON
                solicitudes."idCotizacionAli" = "cotizacionesAli"."id"
            LEFT JOIN
                operaciones."productoCliente"
                ON
                solicitudes."id" = "productoCliente"."idSolicitud"
            LEFT JOIN
                operaciones.registro
                ON
                "productoCliente"."id" = registro."idProducto"
            LEFT JOIN
                operaciones.recibos
                ON
                registro."id" = recibos."idRegistro"
            LEFT JOIN
                operaciones.pagos
                ON
                recibos."id" = pagos."idRecibo"
            WHERE solicitudes.id = ${idSolicitud}
        `, {
            type: QueryTypes.SELECT,
            plain: true
        });
    }

    static async getByIdEstadoContacto(idEstadoContacto): Promise<any> {
        return await dbPostgres.query(`
            SELECT *
                FROM operaciones.solicitudes
                WHERE operaciones.solicitudes."idEstadoContacto" = ${idEstadoContacto}
                    ORDER BY
                    id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    static async post(data) {
        return await solicitudesInternoModel.create(data).then(data => {
           return data.id;
        });
    }

    static async postGM(data) {
        return await solicitudesInternoModel.create(data).then(data => {
            return data;
        }).catch(e=> console.log(e));
    }

    static async update(data, id) {
        return await solicitudesInternoModel.update(data, {
            where: {id}
        });
    }

    static async getInfoProspectoStepCliente(id) {
        return dbPostgres.query(`
        SELECT
            prospecto.numero,
            prospecto.nombre,
            prospecto.correo
        FROM
            operaciones.solicitudes
        INNER JOIN
            operaciones."cotizacionesAli"
            ON
            solicitudes."idCotizacionAli" = "cotizacionesAli"."id"
        INNER JOIN
            operaciones.cotizaciones
            ON
            "cotizacionesAli"."idCotizacion" = cotizaciones."id"
        INNER JOIN
            operaciones."productoSolicitud"
            ON
            cotizaciones."idProducto" = "productoSolicitud"."id"
        INNER JOIN
            operaciones.prospecto
            ON
            "productoSolicitud"."idProspecto" = prospecto."id"
        WHERE solicitudes."id" = ${id}
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        })
    }
// SOLICITUDESVNVIEW
    static async getAllTipo(idTipoContacto, fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            operaciones.solicitudes.id,
            operaciones.solicitudes."idCotizacionAli",
            operaciones.solicitudes."idEmpleado",
            operaciones.solicitudes."idEstadoSolicitud",
            operaciones.solicitudes."fechaSolicitud",
            operaciones.solicitudes."comentarios",
            operaciones."cotizacionesAli"."idCotizacion",
            operaciones."cotizacionesAli"."fechaCotizacion",
            operaciones."estadoSolicitud".estado,
            operaciones."estadoSolicitud".activo,
            "recursosHumanos".empleado."idCandidato",
            "recursosHumanos".departamento.id "idDepartamento",
            "recursosHumanos".departamento.descripcion "descripcionDepartamento",
            CASE WHEN operaciones.solicitudes."idEmpleado" = 1737 THEN 'EN PROCESO DE REASIGNACIÓN' ELSE "recursosHumanos".precandidato.nombre || ' ' || "recursosHumanos".precandidato."apellidoPaterno" || ' ' || "recursosHumanos".precandidato."apellidoMaterno" END as "nombreEmpleado",
            operaciones.cotizaciones."idProducto",
            operaciones.cotizaciones."idTipoContacto",
            operaciones."productoSolicitud"."idProspecto",
            CASE WHEN operaciones."productoSolicitud".datos::json ->> 'nombre' is null then operaciones.prospecto.nombre else operaciones."productoSolicitud".datos::json ->> 'nombre' end AS "nombreProspecto",
            operaciones.prospecto.correo "correoProspecto",
            CASE WHEN operaciones."productoSolicitud".datos::json ->> 'telefono' is null then operaciones.prospecto.numero::TEXT else operaciones."productoSolicitud".datos::json ->> 'telefono' end AS "numeroProspecto",
            CASE WHEN operaciones."productoSolicitud".datos::json ->> 'telefono' is null then operaciones.prospecto.numero::TEXT else operaciones."productoSolicitud".datos::json ->> 'telefono' end AS "numero",
            operaciones.solicitudes."idFlujoSolicitud",
            operaciones.cotizaciones."idMedioDifusion"
            FROM operaciones.solicitudes
            INNER JOIN operaciones."cotizacionesAli" ON operaciones.solicitudes."idCotizacionAli" = operaciones."cotizacionesAli".id
            LEFT JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = operaciones."solicitudes"."idEmpleado"
            LEFT JOIN "recursosHumanos".candidato ON "recursosHumanos".candidato.id = "recursosHumanos"."empleado"."idCandidato"
            LEFT JOIN "recursosHumanos".precandidato ON "recursosHumanos".precandidato.id = "recursosHumanos"."candidato"."idPrecandidato"
            LEFT JOIN "recursosHumanos".plaza ON "recursosHumanos".plaza.id = "recursosHumanos"."empleado"."idPlaza"
            LEFT JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento.id = "recursosHumanos"."plaza"."idDepartamento"
            INNER JOIN operaciones."estadoSolicitud" ON operaciones.solicitudes."idEstadoSolicitud" = operaciones."estadoSolicitud".id
            INNER JOIN operaciones.cotizaciones ON operaciones.cotizaciones."id" = operaciones."cotizacionesAli"."idCotizacion"
            INNER JOIN operaciones."productoSolicitud" ON operaciones."productoSolicitud"."id" = operaciones.cotizaciones."idProducto"
            INNER JOIN operaciones."prospecto" ON "operaciones"."prospecto"."id" = operaciones."productoSolicitud"."idProspecto"
            WHERE
            DATE(operaciones.solicitudes."fechaSolicitud") BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
            AND
            operaciones.cotizaciones."idTipoContacto" IN (${idTipoContacto}, 19)
            ORDER BY
            operaciones.solicitudes.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    // AND
    // operaciones."estadoSolicitud".id IN (${idEstado},${idEstadoGM})
// SOLICITUDESVNVIEW
    static async getAllByDepartamentosTipo(idTipoContacto, fechaInicio, fechaFin,idEstadoGM, ...departamentos): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            operaciones.solicitudes.id,
            operaciones.solicitudes."idCotizacionAli",
            operaciones.solicitudes."idEmpleado",
            operaciones.solicitudes."idEstadoSolicitud",
            operaciones.solicitudes."fechaSolicitud",
            operaciones.solicitudes."comentarios",
            operaciones."cotizacionesAli"."idCotizacion",
            operaciones."cotizacionesAli"."fechaCotizacion",
            operaciones."estadoSolicitud".estado,
            operaciones."estadoSolicitud".activo,
            "recursosHumanos".empleado."idCandidato",
            "recursosHumanos".departamento.id "idDepartamento",
            "recursosHumanos".departamento.descripcion "descripcionDepartamento",
            CASE WHEN operaciones.solicitudes."idEmpleado" = 1737 THEN 'EN PROCESO DE REASIGNACIÓN' ELSE "recursosHumanos".precandidato.nombre || ' ' || "recursosHumanos".precandidato."apellidoPaterno" || ' ' || "recursosHumanos".precandidato."apellidoMaterno" END as "nombreEmpleado",
            operaciones.cotizaciones."idProducto",
            operaciones.cotizaciones."idTipoContacto",
            operaciones."productoSolicitud"."idProspecto",
            CASE WHEN operaciones."productoSolicitud".datos::json ->> 'nombre' is null then operaciones.prospecto.nombre else operaciones."productoSolicitud".datos::json ->> 'nombre' end AS "nombreProspecto",
            operaciones.prospecto.correo "correoProspecto",
            CASE WHEN operaciones."productoSolicitud".datos::json ->> 'telefono' is null then operaciones.prospecto.numero::TEXT else operaciones."productoSolicitud".datos::json ->> 'telefono' end AS "numeroProspecto",
            CASE WHEN operaciones."productoSolicitud".datos::json ->> 'telefono' is null then operaciones.prospecto.numero::TEXT else operaciones."productoSolicitud".datos::json ->> 'telefono' end AS "numero",
            operaciones.solicitudes."idFlujoSolicitud",
            operaciones.cotizaciones."idMedioDifusion"
            FROM operaciones.solicitudes
            INNER JOIN operaciones."cotizacionesAli" ON operaciones.solicitudes."idCotizacionAli" = operaciones."cotizacionesAli".id
            LEFT JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = operaciones."solicitudes"."idEmpleado"
            LEFT JOIN "recursosHumanos".candidato ON "recursosHumanos".candidato.id = "recursosHumanos"."empleado"."idCandidato"
            LEFT JOIN "recursosHumanos".precandidato ON "recursosHumanos".precandidato.id = "recursosHumanos"."candidato"."idPrecandidato"
            LEFT JOIN "recursosHumanos".plaza ON "recursosHumanos".plaza.id = "recursosHumanos"."empleado"."idPlaza"
            LEFT JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento.id = "recursosHumanos"."plaza"."idDepartamento"
            INNER JOIN operaciones."estadoSolicitud" ON operaciones.solicitudes."idEstadoSolicitud" = operaciones."estadoSolicitud".id
            INNER JOIN operaciones.cotizaciones ON operaciones.cotizaciones."id" = operaciones."cotizacionesAli"."idCotizacion"
            INNER JOIN operaciones."productoSolicitud" ON operaciones."productoSolicitud"."id" = operaciones.cotizaciones."idProducto"
            INNER JOIN operaciones."prospecto" ON "operaciones"."prospecto"."id" = operaciones."productoSolicitud"."idProspecto"
            WHERE
            DATE(operaciones.solicitudes."fechaSolicitud") BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
            AND
            operaciones.solicitudes."idDepartamento" IN (182,183,215,138, 58, 134, 135, 136, 137, 139)
            AND
            operaciones.cotizaciones."idTipoContacto" IN (19)
            ORDER BY operaciones.solicitudes.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    // AND
    // operaciones."estadoSolicitud".id IN (${idEstado},${idEstadoGM})
// SOLICITUDESVNVIEW
    static async getAllByDepartamentoAndIdEmpleadoTipo(idDepartamento, idTipoContacto, idEmpleado, fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            operaciones.solicitudes.id,
            operaciones.solicitudes."idCotizacionAli",
            operaciones.solicitudes."idEmpleado",
            operaciones.solicitudes."idEstadoSolicitud",
            operaciones.solicitudes."fechaSolicitud",
            operaciones.solicitudes."comentarios",
            operaciones."cotizacionesAli"."idCotizacion",
            operaciones."cotizacionesAli"."fechaCotizacion",
            operaciones."estadoSolicitud".estado,
            operaciones."estadoSolicitud".activo,
            "recursosHumanos".empleado."idCandidato",
            "recursosHumanos".departamento.id "idDepartamento",
            "recursosHumanos".departamento.descripcion "descripcionDepartamento",
            CASE WHEN operaciones.solicitudes."idEmpleado" = 1737 THEN 'EN PROCESO DE REASIGNACIÓN' ELSE "recursosHumanos".precandidato.nombre || ' ' || "recursosHumanos".precandidato."apellidoPaterno" || ' ' || "recursosHumanos".precandidato."apellidoMaterno" END as "nombreEmpleado",
            operaciones.cotizaciones."idProducto",
            operaciones.cotizaciones."idTipoContacto",
            operaciones."productoSolicitud"."idProspecto",
            CASE WHEN operaciones."productoSolicitud".datos::json ->> 'nombre' is null then operaciones.prospecto.nombre else operaciones."productoSolicitud".datos::json ->> 'nombre' end AS "nombreProspecto",
            operaciones.prospecto.correo "correoProspecto",
            CASE WHEN operaciones."productoSolicitud".datos::json ->> 'telefono' is null then operaciones.prospecto.numero::TEXT else operaciones."productoSolicitud".datos::json ->> 'telefono' end AS "numeroProspecto",
            CASE WHEN operaciones."productoSolicitud".datos::json ->> 'telefono' is null then operaciones.prospecto.numero::TEXT else operaciones."productoSolicitud".datos::json ->> 'telefono' end AS "numero",
            operaciones.solicitudes."idFlujoSolicitud",
            operaciones.cotizaciones."idMedioDifusion"
            FROM operaciones.solicitudes
            INNER JOIN operaciones."cotizacionesAli" ON operaciones.solicitudes."idCotizacionAli" = operaciones."cotizacionesAli".id
            LEFT JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = operaciones."solicitudes"."idEmpleado"
            LEFT JOIN "recursosHumanos".candidato ON "recursosHumanos".candidato.id = "recursosHumanos"."empleado"."idCandidato"
            LEFT JOIN "recursosHumanos".precandidato ON "recursosHumanos".precandidato.id = "recursosHumanos"."candidato"."idPrecandidato"
            LEFT JOIN "recursosHumanos".plaza ON "recursosHumanos".plaza.id = "recursosHumanos"."empleado"."idPlaza"
            LEFT JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento.id = "recursosHumanos"."plaza"."idDepartamento"
            INNER JOIN operaciones."estadoSolicitud" ON operaciones.solicitudes."idEstadoSolicitud" = operaciones."estadoSolicitud".id
            INNER JOIN operaciones.cotizaciones ON operaciones.cotizaciones."id" = operaciones."cotizacionesAli"."idCotizacion"
            INNER JOIN operaciones."productoSolicitud" ON operaciones."productoSolicitud"."id" = operaciones.cotizaciones."idProducto"
            INNER JOIN operaciones."prospecto" ON "operaciones"."prospecto"."id" = operaciones."productoSolicitud"."idProspecto"
            WHERE
            DATE(operaciones.solicitudes."fechaSolicitud") BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
            AND
            operaciones.solicitudes."idEmpleado" = ${idEmpleado}
            AND
            operaciones.cotizaciones."idTipoContacto" IN (${idTipoContacto}, 19)

            ORDER BY operaciones.solicitudes.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    // AND
    // operaciones."estadoSolicitud".id IN (${idEstado},${idEstadoGM})
}
