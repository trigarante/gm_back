import TipoSubRamoModel from "../../models/venta-nueva/solicitudes/steps/producto-cliente/tipoSubRamoModel";
import {Op} from "sequelize";

export default class TipoSubRamoQueries {
    static async getFiltrados() {
        return await TipoSubRamoModel.findAll({
            where: {id: {
                [Op.in]: [1,2,9]
                }}
        })
    }
}
