import CotizacionesModel from "../../models/venta-nueva/cotizador/cotizacionesModel";

export default class CotizacionesQueries {
    static async post(data) {
        return await CotizacionesModel.create(data).then(data => {
            return data['id'];
        });
    }
    static async postGM(data) {
        return await CotizacionesModel.create(data).then(data => {
            return data;
        });
    }
}
