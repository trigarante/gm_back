import PagosModel from "../../models/venta-nueva/pagos/pagosModel";
import {dbPostgres} from "../../configs/connection";
import {QueryTypes} from "sequelize";

export default class PagosQueries {
    static async getById(id): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                    pa.id,
                    pa."idRecibo",
                    pa."idFormaPago",
                    pa."idEmpleado",
                    pa."idEstadoPago",
                    pa."fechaPago",
                    pa.cantidad,
                    pa.archivo,
                    pa.datos,
                    re.numero,
                    re.cantidad AS recibosCantidad,
                    fp.descripcion AS "formapagoDescripcio",
                    pa.datos::json ->>'numeroTarjeta' AS numeroTarjeta,
                    pa.datos::json ->>'titular' AS titular
                    FROM operaciones.pagos pa
                    JOIN operaciones.recibos re ON pa."idRecibo" = re.id
                    JOIN operaciones."formaPago" fp ON pa."idFormaPago" = fp.id
                    JOIN operaciones."estadoPago" ep ON pa."idEstadoPago" = ep.id
            WHERE pa.id = ${id}
        `, {
            type: QueryTypes.SELECT,
            plain: true
        });
    }
    static async getByIdEstado(id): Promise<any> {
        return await PagosModel.findOne({
            where: {id},
            raw: true
        })
    }

    static async post(data){
        return await PagosModel.create(data).then(data => {
            return data['id'];
        })
    }

    static async update(data, id) {
        return await PagosModel.update(data, {
            where: {id}
        })
    }
    static async updateEstado(idEstadoPago: number, data): Promise<any> {
        return await PagosModel.update({...data}, {
            where: {
                id: idEstadoPago,
            }
        });
    }
}
