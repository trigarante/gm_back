import BancosModel from "../../models/venta-nueva/bancos/BancosModel";


export default class BancosQueries {

    static async findAll(): Promise<any> {
        return await BancosModel.findAll({
            where: {activo: 1}
        })
    }

}



