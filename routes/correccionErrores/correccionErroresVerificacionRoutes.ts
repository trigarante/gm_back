import {Router, Request, Response} from "express";
import UsuariosDepartamentoQueries from "../../queries/usuariosDepartamento/usuariosDepartamentoQueries";
import CorreccionErroresVerificacionQueries from "../../queries/correccionErrores/CorreccionErroresVerificacionQueries";
const generales = require('../../general/function');
const correccionErroresVerificacionRoutes = Router();

correccionErroresVerificacionRoutes.get('/anexo/:idEmpleado/:fechaInicio/:fechaFin', async(req:Request, res: Response, next) => {
    try {
        const {idEmpleado,fechaInicio, fechaFin} = req.params;
        if (!idEmpleado) return generales.manejoErrores('No se envio el id empleado', res);
        const permisos = await CorreccionErroresVerificacionQueries.getPermisosVisualizacion(+idEmpleado);
        let autorizacionRegistro;
        switch (+permisos.idPermisoVisualizacion) {
            case 1:
                autorizacionRegistro = await CorreccionErroresVerificacionQueries.getAll(fechaInicio, fechaFin)
                break;
            case 2:
                const departamentos = await UsuariosDepartamentoQueries.getDepartamentoByIdUsuario(permisos.idUsuario)
                let aux = [];
                for (const departamento of departamentos) {
                    aux.push(departamento.idDepartamento)
                }
                if (departamentos.length === 0){
                    aux.push(permisos.idDepartamento)
                }
                autorizacionRegistro = await CorreccionErroresVerificacionQueries.getAllByDepartamentos(fechaInicio, fechaFin,aux)
                break;
            case 3:
                autorizacionRegistro = await CorreccionErroresVerificacionQueries.getAllByDepartamentoAndIdEmpleado(permisos.idDepartamento, idEmpleado, fechaInicio, fechaFin)
                break;
        }
        res.status(200).send( autorizacionRegistro );
    } catch (e) {
        res.status(500).send(e)
    }
})
correccionErroresVerificacionRoutes.get('/datos/:idEmpleado/:fechaInicio/:fechaFin', async(req:Request, res: Response, next) => {
    try {
        const {idEmpleado,fechaInicio, fechaFin} = req.params;
        if (!idEmpleado) return generales.manejoErrores('No se envio el id empleado', res);
        const permisos = await CorreccionErroresVerificacionQueries.getPermisosVisualizacion(+idEmpleado);
        let autorizacionRegistro;
        switch (+permisos.idPermisoVisualizacion) {
            case 1:
                autorizacionRegistro = await CorreccionErroresVerificacionQueries.getAllDatos(fechaInicio, fechaFin)
                break;
            case 2:
                const departamentos = await UsuariosDepartamentoQueries.getDepartamentoByIdUsuario(permisos.idUsuario)
                let aux = [];
                for (const departamento of departamentos) {
                    aux.push(departamento.idDepartamento)
                }
                autorizacionRegistro = await CorreccionErroresVerificacionQueries.getAllByDepartamentosDatos(fechaInicio, fechaFin,aux)
                break;
            case 3:
                autorizacionRegistro = await CorreccionErroresVerificacionQueries.getAllByDepartamentoAndIdEmpleadoDatos(permisos.idDepartamento, idEmpleado, fechaInicio, fechaFin)
                break;
        }
        res.status(200).send( autorizacionRegistro );
    } catch (e) {
        res.status(500).send(e)
    }
})

export default correccionErroresVerificacionRoutes;
