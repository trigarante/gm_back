import {Request, Response, Router} from "express";
import generales from "../../general/function";
import SubRamoQueries from "../../queries/subramo/subRamoQueries";

const router = Router();

router.get('/getByIdRamo', async (req: Request, res: Response, next) => {
    try {
        const {id} = req.headers;
        if (!id) return generales.manejoErrores('No se envio ningun id', res);
        const ramos = await SubRamoQueries.getByIdRamo(id);
        res.status(200).send(ramos);
    } catch (err) {
        next(err);
    }
})

export default router;
