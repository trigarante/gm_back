import {Router, Request, Response} from "express";
import ClienteQueries from "../../queries/cliente/cliente-queries";
const generales = require('../../general/function');

const clienteRoutes = Router();

clienteRoutes.get('/byId/:id', async(req:Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id ) return generales.manejoErrores('No se envio el id Cliente', res);
        const cliente = await ClienteQueries.clienteAll(id);
        res.status(200).send( cliente );
    } catch (e) {
        res.sendStatus(500);
    }
});

clienteRoutes.get('/existeCurp', async(req:Request, res: Response, next) => {
    try {
        const {curp} = req.headers;
        if (!curp ) return generales.manejoErrores('No se envio el Curp', res);
        const cliente = await ClienteQueries.getByCurp(curp);
        res.status(200).send(cliente);
    } catch (e) {
        res.sendStatus(500);
    }
});

clienteRoutes.get('/existeRFC', async(req:Request, res: Response, next) => {
    try {
        const {rfc} = req.headers;
        if (!rfc ) return generales.manejoErrores('No se envio el RFC', res);
        const cliente = await ClienteQueries.getByRFC(rfc);
        res.status(200).send(cliente);
    } catch (e) {
        res.sendStatus(500);
    }
});

clienteRoutes.post('', async(req:Request, res: Response, next) => {
    try {
        console.log(req.body)
        const id = await ClienteQueries.post(req.body);
        res.status(200).send({id});
    } catch (e) {
        res.sendStatus(500);
    }
});

clienteRoutes.put('/:idCliente', async(req:Request, res: Response, next) => {
    try {
        const {idCliente} = req.params;
        if (!idCliente) return generales.manejoErrores('No se envio el id', res);
        await ClienteQueries.update(req.body, idCliente)
        res.status(200).send()
    } catch (e) {
        res.sendStatus(500);
    }
});

export default clienteRoutes;
