import {Request, Response, Router} from "express";
import ErroresAnexosVQueries from "../../queries/erroresAnexosVerificacion/erroresAnexosVQueries";
const generales = require('../../general/function');
const erroresAnexosVRoutes = Router();

erroresAnexosVRoutes.get('/:idVerificacionRegistro/:idTipoDocumento', async (req: Request, res: Response, next) => {
    try {
        const {idVerificacionRegistro,idTipoDocumento} = req.params;
        if (!(idVerificacionRegistro && idTipoDocumento)) return generales.manejoErrores('No se envio el id', res);
        const tiposDocumentos = await ErroresAnexosVQueries.getByIdAutorizacionAndDocumento(idVerificacionRegistro, idTipoDocumento);
        res.status(200).send(tiposDocumentos);
    } catch (err) {
        next(err);
    }
})
// erroresAnexosVRoutes.post('/', async (req: Request, res: Response, next) => {
//     try {
//         await ErroresAnexosAQueries.post(req.body)
//         return res.status(200).send();
//     } catch (err) {
//         next(err)
//     }
// });

export default erroresAnexosVRoutes;

