import {Request, Response, Router} from "express";
import CotizacionesAliQueries from "../../queries/cotizador/cotizacionesAliQueries";
const generales = require('../../general/function');
const cotizacionesAliRoutes = Router();

cotizacionesAliRoutes.get('/:id', async (req: Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id) return generales.manejoErrores('No se envio el id del Empleado', res);
        const empleado = await CotizacionesAliQueries.getById(id);
        res.status(200).send(empleado);
    } catch (err) {
        next(err);
    }
})

cotizacionesAliRoutes.post('', async (req: Request, res: Response, next) => {
    try {
        // req.body
        const cotizacionAli = await CotizacionesAliQueries.postGM(req.body);
        res.status(200).send(cotizacionAli);
    } catch (err) {
        next(err);
    }
})


export default cotizacionesAliRoutes;

