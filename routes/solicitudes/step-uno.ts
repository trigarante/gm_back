import {Request, Response, Router} from "express";
import CrearClienteQueries from "../../queries/solicitudes/crear-cliente/crear-cliente-queries";
import ClienteModel from "../../models/venta-nueva/solicitudes/steps/crear-cliente/cliente-model";
const generales = require('../../general/function');

const stepUnoRoutes = Router();

stepUnoRoutes.get('/solicitudes-vn/get-by-id-sin-json', async (req: Request, res: Response, next) => {
    const idSolicitud = Number(req.headers.idsolicitud);
    if (!idSolicitud)
        return generales.manejoErrores('No se envió idSolicitud', res);
    const solicitudesData = await CrearClienteQueries.getByIdSinJson(idSolicitud);
    console.log('algo', solicitudesData);
    res.status(200).send( solicitudesData );
});

stepUnoRoutes.get('/cliente/curp', async (req: Request, res: Response, next) => {
    try {
        const curp = (req.headers.curp);
        if (!curp)
            return generales.manejoErrores('No se envió curp', res);
        const clienteData = await CrearClienteQueries.findByCurp(curp);
        res.status(200).send( clienteData );
    } catch (e) {
        res.status(500).send( e );
    }
});

stepUnoRoutes.get('/cliente/rfc', async (req: Request, res: Response, next) => {
    const rfc = req.headers.rfc;
    if (!rfc)
        return generales.manejoErrores('No se envió rfc', res);
    const clienteData = await CrearClienteQueries.findByrfc(rfc);
    res.status(200).send( clienteData );
});

stepUnoRoutes.get('/paises', async (req: Request, res: Response, next) => {
    try {
        const paisesData = await CrearClienteQueries.getPaises();
        res.status(200).send( paisesData );
    } catch (e) {
        res.status(500).send( e );
    }
});



stepUnoRoutes.get('/cliente', async (req: Request, res: Response, next) => {
    try {
        const idCliente = +req.headers.idcliente;
        if (!idCliente)
            return generales.manejoErrores('No se envió idCliente', res);
        const clienteData = await CrearClienteQueries.getCLienteById(idCliente);
        res.status(200).send( clienteData );
    } catch (e) {
        res.status(500).send( e );
    }
});

// stepUnoRoutes.post('/cliente', async (req: Request, res: Response, next) => {
//     try {
//         const nuevoCliente = await ClienteModel.create({...req.body}, {
//             fields: ['idPais', 'razonSocial', 'nombre', 'paterno', 'materno', 'cp', 'calle', 'numInt', 'numExt', 'idColonia',
//                 'genero', 'telefonoFijo', 'telefonoMovil', 'correo', 'curp', 'rfc', 'archivo', 'archivoSubido']
//         });
//         if (nuevoCliente) {
//             console.log(nuevoCliente);
//             res.status(200).send( nuevoCliente );
//         }
//     } catch (e) {
//         console.log(e);
//         res.status(500).send( e );
//     }
// });

stepUnoRoutes.put('/cliente', async (req: Request, res: Response, next) => {
    const idCliente = Number(req.headers.idcliente);
    const idEmpleado = Number(req.headers.idempleado);
    if (!idCliente || !idEmpleado)
        return  generales.manejoErrores('No se enviaron los parámetros requeridos', res);
    const clienteActuaizado = CrearClienteQueries.updateCliente(idCliente, {...req.body});
    res.status(200).send( clienteActuaizado );
});



export default stepUnoRoutes;
