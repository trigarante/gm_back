import {Request, Response, Router} from "express";
import RegistroPolizaQueries from "../../queries/solicitudes/registro-poliza-queries";
import CrearClienteQueries from "../../queries/solicitudes/crear-cliente/crear-cliente-queries";
import stepUnoRoutes from "./step-uno";
const generales = require('../../general/function');

const registroPolizaRoutes = Router();

registroPolizaRoutes.get('/productos-socio/id-subramo', async (req: Request, res: Response, next) => {
    const idSubramo = Number(req.headers.idsubramo);
    if (!idSubramo)
        return generales.manejoErrores('No se envió idSubramo', res);
    const subramo = await RegistroPolizaQueries.findAllByIdSubRamoAndActivo(idSubramo);
    res.status(200).send( subramo );
});
registroPolizaRoutes.get('/tipo-pago', async (req: Request, res: Response, next) => {
    const tipoPago = await RegistroPolizaQueries.findAllByIdLessThanAndActivo();
    res.status(200).send( tipoPago );
});
registroPolizaRoutes.get('/registro', async (req: Request, res: Response, next) => {
    const idRegistro = +req.headers.idregistro;
    if (!idRegistro)
        return generales.manejoErrores('No se envió idRegistro', res);
    const registro = await RegistroPolizaQueries.findRegistroById(idRegistro);
    res.status(200).send( registro );
});
registroPolizaRoutes.get('/registro-vn/existe-poliza', async (req: Request, res: Response, next) => {
    const poliza = req.body.poliza;
    const idSocio = req.body.idSocio;
    const mes = req.body.mes;
    const anio = req.body.anio;
    const registro = await RegistroPolizaQueries.buscarPoliza(poliza, idSocio, mes, anio);
    res.status(200).send( registro );
});
registroPolizaRoutes.get('/recibovn/registro', async (req: Request, res: Response, next) => {
    const idRegistro = +req.headers.idregistro;
    if (!idRegistro)
        return generales.manejoErrores('No se envió idRegistro', res);
    const registro = await RegistroPolizaQueries.findAllByIdRegistroAndActivo(idRegistro);
    res.status(200).send( registro );
});
registroPolizaRoutes.get('/sepomex', async (req: Request, res: Response, next) => {
    try {
        const cp = req.headers.cp;
        if (!cp)
            return generales.manejoErrores('No se envió cp', res);
        const coloniaData = await CrearClienteQueries.getColoniaByCp(cp);
        res.status(200).send( coloniaData );
    } catch (e) {
        res.status(500).send( e );
    }
});

export default registroPolizaRoutes;
