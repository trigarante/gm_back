import {Request, Response, Router} from "express";

const DashboardRoutes = Router();

// dashboard's routes
DashboardRoutes.get('/get-dashboard-filter-pjsip/:idEmpleado/:fechaInicio/:fechaFin', async (req: Request, res: Response, next) => {
    try {

    } catch (err) {
        next(err);
    }
});
DashboardRoutes.get('/get-dashboard-filter-reno-pjsip/:idEmpleado/:fechaInicio/:fechaFin', async (req: Request, res: Response, next) => {
    try {

    } catch (err) {
        next(err);
    }
});
DashboardRoutes.get('/get-dashboard-pjsip/:idEmpleado/:fechaInicio/:fechaFin/:idSubarea', async (req: Request, res: Response, next) => {
    try {

    } catch (err) {
        next(err);
    }
});
DashboardRoutes.get('/get-descarga-llamadas/:idEmpleado/:fechaInicio/:fechaFin', async (req: Request, res: Response, next) => {
    try {

    } catch (err) {
        next(err);
    }
});
DashboardRoutes.get('/get-descarga-leads/:idEmpleado/:fechaInicio/:fechaFin', async (req: Request, res: Response, next) => {
    try {

    } catch (err) {
        next(err);
    }
});
DashboardRoutes.get('/get-descarga-dash/:idEmpleado/:fechaInicio/:fechaFin/:tipo', async (req: Request, res: Response, next) => {
    try {

    } catch (err) {
        next(err);
    }
});
export default DashboardRoutes;
