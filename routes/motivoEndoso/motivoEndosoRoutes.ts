import {Router, Request, Response} from "express";
import MotivoEndosoQueries from "../../queries/motivoEndoso/MotivoEndosoQueries";
const generales = require('../../general/function');

const motivoEndosoRoutes = Router();

motivoEndosoRoutes.get('/:id', async(req:Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id ) return generales.manejoErrores('No se envio el id Cliente', res);
        const motivoEndoso = await MotivoEndosoQueries.findAllByIdTipoEndoso(id);
        res.status(200).send( motivoEndoso );
    } catch (e) {
        res.status(500).send(e)
    }
})


export default motivoEndosoRoutes;
