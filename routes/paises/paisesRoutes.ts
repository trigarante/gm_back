import {Request, Response, Router} from "express";
import PaisesQueries from "../../queries/paises/paisesQueries";

const router = Router();

router.get('', async(req:Request, res: Response, next) => {
    try {
        const paises = await PaisesQueries.getActivo();
        res.status(200).send(paises);
    } catch (e) {
        res.status(500).send(e)
    }
});

router.get('', async(req:Request, res: Response, next) => {
    try {
        const paises = await PaisesQueries.getActivo();
        res.status(200).send(paises);
    } catch (e) {
        res.status(500).send(e)
    }
});

export default router;
