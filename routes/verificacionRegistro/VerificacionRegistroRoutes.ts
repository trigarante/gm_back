import {Request, Response, Router} from "express";
import UsuariosDepartamentoQueries from "../../queries/usuariosDepartamento/usuariosDepartamentoQueries";
import VerificacionRegistroQueries from "../../queries/verificacionRegistro/verificacionRegistroQueries";
import PagosQueries from "../../queries/pagos/pagosQueries";
import RegistroQueries from "../../queries/registro/RegistroQueries";
import AutorizacionRegistroQueries
    from "../../queries/AutorizacionErrores/autorizacionRegistro/autorizacionRegistroQueries";
import AdministradorPolizasQueries from "../../queries/adminPolizas/administradorPolizas-queries";
import PlazaQueries from "../../queries/RRHH/plazaQueries";
const generales = require('../../general/function');
const VerificacionRegistroRoutes = Router();

VerificacionRegistroRoutes.get('/pendientes/:idEmpleado/:fechaInicio/:fechaFin', async(req:Request, res: Response, next) => {
    try {
        const {idEmpleado,fechaInicio, fechaFin} = req.params;
        if (!idEmpleado) return generales.manejoErrores('No se envio el id empleado', res);
        const permisos = await AdministradorPolizasQueries.getPermisosVisualizacion(+idEmpleado);
        let verificacionRegistro;
        switch (+permisos.idPermisoVisualizacion) {
            case 1:
                verificacionRegistro = await VerificacionRegistroQueries.getAll(fechaInicio, fechaFin)
                break;
            case 2:
                // if (+permisos.idPuesto === 7) {
                //     // Supervisores solo deben de ver lo de sus chavos
                //     let hijos = await PlazaQueries.getPlazasHijo(permisos.id);
                //     let aux = [];
                //     for (const hijo of hijos) {
                //         aux.push(hijo.id)
                //     }
                //     if (hijos.length === 0){
                //         aux = [+idEmpleado]
                //     }
                //     verificacionRegistro = await VerificacionRegistroQueries.getAllByHijos(fechaInicio, fechaFin, aux)
                // } else {
                    const departamentos = await UsuariosDepartamentoQueries.getDepartamentoByIdUsuario(permisos.idUsuario)
                    let aux = [];
                    for (const departamento of departamentos) {
                        aux.push(departamento.idDepartamento)
                    }
                if (departamentos.length === 0){
                    aux.push(permisos.idDepartamento)
                }
                    verificacionRegistro = await VerificacionRegistroQueries.getAllByDepartamentos(fechaInicio, fechaFin, aux)
                // }
                break;
            case 3:
                verificacionRegistro = await VerificacionRegistroQueries.getAllByDepartamentoAndIdEmpleado(permisos.idDepartamento, idEmpleado, fechaInicio, fechaFin)
                break;
        }
        res.status(200).send( verificacionRegistro );
    } catch (e) {
        res.status(500).send(e)
    }
})
VerificacionRegistroRoutes.get('/get-by-id/:id', async (req: Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id) return generales.manejoErrores('No se envio ningun id', res);
        const byId = await VerificacionRegistroQueries.getById(id);
        res.status(200).send(byId);
    } catch (err) {
        next(err);
    }
})

VerificacionRegistroRoutes.post('', async (req: Request, res: Response, next) => {
    try {
        await VerificacionRegistroQueries.postVerificacionRegistro(req.body)
        await AutorizacionRegistroQueries.updateByIdRegistro({idEstadoVerificacion: 3, fechaConclusion: new Date()} ,req.body.idRegistro)
        await RegistroQueries.update({idEstadoPoliza: 5}, req.body.idRegistro);
        return res.status(200).send();
    } catch (e) {
        res.status(500).send(e)
    }
});

VerificacionRegistroRoutes.put('/documento', async(req:Request, res: Response, next) => {
    try {
        const idVerificacionRegistro = Number(req.headers.idverificacionregistro);
        const idDocumentoVerificado = Number(req.headers.iddocumentoverificado);
        const estado = Number(req.headers.estado);
        console.log(req.headers)
        if (!(idVerificacionRegistro && idDocumentoVerificado && estado)) return generales.manejoErrores('No se enviaron los parametros requeridos', res);
        const autorizacion = await VerificacionRegistroQueries.getByIdEstado(idVerificacionRegistro)
        switch (+idDocumentoVerificado) {
            case 1:
                autorizacion.verificadoCliente = estado
                break;
            case 2:
                autorizacion.verificadoRegistro = estado
                break;
            case 3:
                autorizacion.verificadoProducto = estado
                break;
            case 4:
                autorizacion.verificadoPago = estado
                break
        }
        if (estado === 4 && idDocumentoVerificado === 4) {
            const verificacionPago = VerificacionRegistroQueries.getById(idVerificacionRegistro)
            if (verificacionPago.idEstadoPago === 2){
                PagosQueries.updateEstado(verificacionPago.idPago, {
                   idEstadoPago : 1
                });
            }
        }

        if (autorizacion.idEstadoVerificacion === 1) {
            autorizacion.idEstadoVerificacion = 2
        }

        if (autorizacion.verificadoCliente === 4 && autorizacion.verificadoRegistro === 4
            && autorizacion.verificadoProducto === 4 && autorizacion.verificadoPago === 4) {
            RegistroQueries.updateEstado(autorizacion.idRegistro, {
                idEstadoPoliza : 6
            });
            autorizacion.idEstadoVerificacion = 3
            autorizacion.fechaConclusion = new Date ();
        }

        const verificacionRegistro = VerificacionRegistroQueries.updateEstado(idVerificacionRegistro, autorizacion);

        res.status(200).send(verificacionRegistro);
    } catch (e) {
        res.status(500).send(e)
    }
});

export default VerificacionRegistroRoutes;
