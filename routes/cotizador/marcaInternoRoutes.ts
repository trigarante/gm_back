import {Request, Response, Router} from "express";
import MarcaInternoQueries from "../../queries/cotizador/marcaInternoQueries";
const generales = require('../../general/function')


const router = Router();

router.get('/getByIdTipoSubramo', async(req:Request, res: Response, next) => {
    try {
        const {id} = req.headers;
        if (!id) return generales.manejoErrores('No se envio ningun id', res);
        const marcas = await MarcaInternoQueries.getByIdTipoSubRamo(id)
        res.status(200).send(marcas);
    } catch (e) {
        res.status(500).send(e)
    }
})

export default router;
