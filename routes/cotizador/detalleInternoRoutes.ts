import {Request, Response, Router} from "express";
import generales from "../../general/function";
import DetalleInternoQueries from "../../queries/cotizador/detalleInternoQueries";

const router = Router();

router.get('/getByIdMarcaAndIdModelo', async(req:Request, res: Response, next) => {
    try {
        const {idmarca, idmodelo} = req.headers;
        if (!(idmarca && idmodelo)) return generales.manejoErrores('No se envio ningun id', res);
        const detalles = await DetalleInternoQueries.getByIdMarcaAndIdModelo(idmarca, idmodelo)
        res.status(200).send(detalles);
    } catch (e) {
        res.status(500).send(e)
    }
})

router.get('/getByIdMarcaAndIdModeloAndIdDescripcion', async(req:Request, res: Response, next) => {
    try {
        const {idmarca, idmodelo, iddescripcion} = req.headers;
        if (!(idmarca && idmodelo && iddescripcion)) return generales.manejoErrores('No se envio ningun id', res);
        const detalles = await DetalleInternoQueries.getByIdMarcaAndIdModeloAndIdDescripcion(idmarca, idmodelo, iddescripcion)
        res.status(200).send(detalles);
    } catch (e) {
        res.status(500).send(e)
    }
})

router.get('/getByIdMarcaAndIdModeloAndIdDescripcionAndIdSubDescripcion', async(req:Request, res: Response, next) => {
    try {
        const {idmarca, idmodelo, iddescripcion, idsubdescripcion} = req.headers;
        if (!(idmarca && idmodelo && iddescripcion && idsubdescripcion)) return generales.manejoErrores('No se envio ningun id', res);
        const detalles = await DetalleInternoQueries.getByIdMarcaAndIdModeloAndIdDescripcionAndIdSubDescripcion(idmarca, idmodelo, iddescripcion, idsubdescripcion)
        res.status(200).send(detalles);
    } catch (e) {
        res.status(500).send(e)
    }
})

export default router;
