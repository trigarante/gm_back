import {Request, Response, Router} from 'express';
import fs from "fs";
import DriveApiService from "../services/drive/driveApiService";
import jwtAuthentica from "../middlewares/jwtAuthentica";

const generales = require('../general/function');
import stepUnoRoutes from "./solicitudes/step-uno";
import productoClienteRoutes from "./solicitudes/producto-cliente-routes";
import registroPolizaRoutes from "./solicitudes/registro-poliza-routes";
import adminPolizasRoutes from "./adminPolizas/adminPolizas-routes";
import solicitudesCarruselRoutes from "./solicitudesDesactivacion/solicitudes-carrusel-routes";
import solicitudesInternoRoutes from "./solicitudesInterno/solicitudes-interno-routes";
import clienteRoutes from "./cliente/cliente-routes";
import ProspectoRoutes from "./prospecto/prospectoRoutes";
import stepCotizadorRoutes from "./step-cotizador/step-cotizador-routes";
import ProductoSolicitudRoutes from "./prospecto/productoSolicitudRoutes";
import TipoSubRamoRoutes from "./cotizador/tipoSubRamoRoutes";
import MarcaInternoRoutes from "./cotizador/marcaInternoRoutes";
import ModeloInternoRoutes from "./cotizador/modeloInternoRoutes";
import DetalleInternoRoutes from "./cotizador/detalleInternoRoutes";
import tipoEndosoRoutes from "./tipoEndoso/tipoEndosoRoutes";
import motivoEndosoRoutes from "./motivoEndoso/motivoEndosoRoutes";
import reciboRoutes from "./recibo/reciboRoutes";
import formaPagoRoutes from "./formaPago/formaPagoRoutes";
import bancosRoutes from "./banco/bancosRoutes";
import registroRoutes from "./registro/registroRoutes";
import TipoPagoRoutes from "./tipoPago/tipoPagoRoutes";
import PeriodicidadRoutes from "./periodicidad/periodicidadRoutes";
import SociosRoutes from "./socios/sociosRoutes";
import RamoRoutes from "./ramo/ramoRoutes";
import SubRamoRoutes from "./subRamo/subRamoRoutes";
import ProductosSocioRoutes from "./productosSocio/productosSocioRoutes";
import estadoInspeccionRoutes from "./estadoInspeccion/estadoInspeccionRoutes";
import PagoRoutes from "./pago/pagoRoutes";
import autorizacionRegistroRoutes from "./autorizacionErrores/autorizacionRegistro/autorizacionRegistroRoutes";
import tipoDocumentosRoutes from "./tipoDocumentos/tipoDocumentosRoutes";
import campoCorregirRoutes from "./campoCorregir/campoCorregirRoutes";
import PaisesRoutes from "./paises/paisesRoutes";
import correccionErroresAutorizacionRoutes from "./correccionErrores/correccionErroresAutorizacionRoutes";
import correccionErroresVerificacionRoutes from "./correccionErrores/correccionErroresVerificacionRoutes";
import erroresAnexosRoutes from "./erroresAnexo/erroresAnexosRoutes";
import SubidaArchivosRoutes from "./subidaArchivos/subidaArchivosRoutes";
import InspeccionesRoutes from "./inspecciones/inspeccionesRoutes";
import VerificacionRegistroRoutes from "./verificacionRegistro/VerificacionRegistroRoutes";
import erroresAutorizacionRoutes from "./autorizacionErrores/erroresAutorizacion/erroresAutorizacionRoutes";
import solicitudEndosoRoutes from "./solicitudEndoso/solicitudEndosoRoutes";
import erroresAnexosVRoutes from "./erroresAnexoVerificacion/erroresAnexosVRoutes";
import erroresVerificacionRoutes from "./erroresVerificacion/erroresVerificacionRoutes";
import empleadosRoutes from "./empleados/empleadosRoutes";
import cotizacionesRoutes from "./cotizaciones/contizaciones";
import cotizacionesAliRoutes from "./cotizacionesAli/cotizacionesAliRoutes";

import DesarrolloOrganizacionalRoutes from "./desarrolloOrganizacional/DesarrolloOrganizacionalRoutes";
import DescargasRoutes from "./descargas/DescargasRoutes";
import DashboardRoutes from "./dashboard/DashboardRoutes";
import MotivosDesactivacionRouter from "./activacionesEmpleado/motivosDesactivacionRoutes";
import SolicitudesCarruselRoutes from "./activacionesEmpleado/solicitudesCarruselRoutes";
import UsuariosRoutes from "./usuarios/usuariosRoutes";
import ProductividadRoutes from "./productividad/productividadRoutes";
import AdministracionPersonalRouter from "./administracion-personal/administracionPersonalRouter";
const jwt = require('jsonwebtoken');

const routes = Router();

// Ruta de acceso denegado
routes.get('', jwtAuthentica, async (request: Request, response: Response) => {
    response.writeHead(200, {'Content-Type': 'text/html'});
    fs.readFile('src/index.html', null, (err, data) => {
        if (err) {
            response.writeHead(404);
            response.write('No está el archivo y eso me da ansiedad D:');
        } else {
            response.write(data);
        }
        response.end();
    });
});

// Ruta de autenticacion JWT
routes.post('/autenticar', async (request: Request, response: Response, next) => {
    try {
        const token = jwt.sign({check: true, idEmpleado: 9159}, process.env.llave, {
            expiresIn: 1440,
        })
        response.status(200).send({mensaje: "Autenticacion correcta", token});
    } catch (e) {
        next(e);
    }
});

// Ruta de prueba
routes.get('/prueba', jwtAuthentica,async (request: Request, response: Response, next) => {
    try {
        console.log(request.headers.idempleado)
        const prueba = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjaGVjayI6dHJ1ZSwiaWF0IjoxNjMzNDc3NDIwLCJleHAiOjE2MzM0Nzg4NjB9.1z8_itpwZH4I448DCLClw104Vw8NWn4CsKiGUCwqbBY';
        console.log(jwt.verify(prueba, process.env.llave))
        // console.log(jwt.verify(prueba, process.env.PORT))
        // const tok = request.headers.token;
        // console.log(request.headers)
        const token = jwt.sign({nombre: 'Juan', apellido: 'Perez'}, process.env.PORT, {
            expiresIn: 1440,
        })
        response.status(200).send(token);
    } catch (e) {
        next(e);
    }
});

// rutas
routes.use('/', stepUnoRoutes);
routes.use('/producto-cliente', productoClienteRoutes);
routes.use('/', registroPolizaRoutes);
routes.use('/', adminPolizasRoutes);
routes.use('/', solicitudesCarruselRoutes);
routes.use('/solicitudes-vn', solicitudesInternoRoutes);
routes.use('/cliente', clienteRoutes);
routes.use('/tipo-endoso', tipoEndosoRoutes);
routes.use('/motivo-endoso', motivoEndosoRoutes);
routes.use('/recibo', reciboRoutes);
routes.use('/registro-vn', registroRoutes);
routes.use('/forma-pago', formaPagoRoutes);
routes.use('/prospecto', ProspectoRoutes);
routes.use('/producto-solicitud', ProductoSolicitudRoutes);
routes.use('/tipoSubRamo', TipoSubRamoRoutes);
routes.use('/marca-interno', MarcaInternoRoutes);
routes.use('/modelo-interno', ModeloInternoRoutes);
routes.use('/detalle-interno', DetalleInternoRoutes);
routes.use('/step-cotizador', stepCotizadorRoutes);
routes.use('/bancos', bancosRoutes);
routes.use('/tipo-pago', TipoPagoRoutes);
routes.use('/periodicidad', PeriodicidadRoutes);
routes.use('/socios', SociosRoutes);
routes.use('/ramo', RamoRoutes);
routes.use('/subRamo', SubRamoRoutes);
routes.use('/producto-socio', ProductosSocioRoutes);
routes.use('/estado-inspeccion', estadoInspeccionRoutes);
routes.use('/pago', PagoRoutes);
routes.use('/autorizacion-registro', autorizacionRegistroRoutes);
routes.use('/tipo-documentos', tipoDocumentosRoutes);
routes.use('/campo-corregir', campoCorregirRoutes);
routes.use('/paises', PaisesRoutes);
routes.use('/correcciones', correccionErroresAutorizacionRoutes);
routes.use('/correccion-errores-verificacion', correccionErroresVerificacionRoutes);
routes.use('/errores-anexos', erroresAnexosRoutes);
routes.use('/subida-archivos', SubidaArchivosRoutes);
routes.use('/inspecciones', InspeccionesRoutes);
routes.use('/verificacion-registro', VerificacionRegistroRoutes);
routes.use('/errores-autorizacion', erroresAutorizacionRoutes);
routes.use('/solicitud-endoso', solicitudEndosoRoutes);
routes.use('/errores-anexos-verificacion', erroresAnexosVRoutes);
routes.use('/errores-verificacion', erroresVerificacionRoutes);
routes.use('/empleados', empleadosRoutes);
routes.use('/cotizaciones', cotizacionesRoutes);
routes.use('/cotizacionesAli', cotizacionesAliRoutes);
routes.use("/desarrollo-organizacional", DesarrolloOrganizacionalRoutes);
routes.use("/dashboard-new", DashboardRoutes );
routes.use("/descargas", DescargasRoutes );
routes.use("/motivos-desactivacion", MotivosDesactivacionRouter);
routes.use("/solicitudes-carrusel", SolicitudesCarruselRoutes);
routes.use("/usuarios", UsuariosRoutes);
routes.use("/productividad", ProductividadRoutes);
// rutas vacaciones
routes.use("/administracion-personal", AdministracionPersonalRouter);
// Subida de archivos a drive producto cliente
routes.post('/subir-archivo', async (req: Request, res: Response, next) => {
    try {
        if (!(req['files'] && req['files'].file)) return generales.manejoErrores('No se envio ningún archivo', res);
        // Recibo archivo
        const files = req['files'].file;
        /* Se deben enviar 2 id, el de cliente y producto cliente
        la carpreta del cliente se creara con el id cliente
        y dentro se guardara un idProductoCliente-cliente para tener las carpetas de cliente pero con
        varios productos cliente
         */
        const {idcliente, idproducto} = req.headers;
        if (!(idcliente  && idproducto)) return generales.manejoErrores('No se envio el id', res);
        const idFolder = await DriveApiService.findOrCreateFolder(1, idcliente);
        const id = await DriveApiService.subirArchivo(idFolder, files, idproducto + '-cliente')
        return res.status(200).send({id});
        // return res.status(200).send();
    } catch (err) {
        next(err)
    }
});

// Subida de archivos a drive registro, pago, inspeccion etc
routes.post('/subir-archivo-otro', async (req: Request, res: Response, next) => {
    try {
        if (!(req['files'] && req['files'].file)) return generales.manejoErrores('No se envio ningún archivo', res);
        // Recibo archivo
        const files = req['files'].file;
        /* Se deben enviar 3 id, el cliente, el del registro y del pago, inspeccion, etc
         */
        const {idpago, idcliente, idregistro} = req.headers;
        if (!(idpago  && idcliente && idregistro)) return generales.manejoErrores('No se envio el id', res);
        const idFolder = await DriveApiService.findOrCreateFolder(1, idcliente);
        const idFolderPago = await DriveApiService.findOrCreateFolder(2, idregistro, idFolder);
        const id = await DriveApiService.subirArchivo(idFolderPago, files, idpago + '-pago')
        return res.status(200).send({id});
        // return res.status(200).send();
    } catch (err) {
        next(err)
    }
});

// Descargar archivo
routes.get('/archivo', async (req: Request, res: Response, next) => {
    try {
        const {archivoid} = req.headers;
        if (!archivoid) return generales.manejoErrores('No se envió el id de la carpeta', res);
        const file = await DriveApiService.obtenerArchivo(archivoid);
        return res.status(200).send([file]);
    } catch (err) {
        next(err)
    }
});

export default routes;
