import {Request, Response, Router} from "express";
import AdministradorPolizasQueries from "../../queries/adminPolizas/administradorPolizas-queries";
import EmpleadosQueries from "../../queries/empleados/empleadosQueries";
import UsuariosDepartamentoQueries from "../../queries/usuariosDepartamento/usuariosDepartamentoQueries";
const generales = require('../../general/function');
const empleadosRoutes = Router();

empleadosRoutes.get('/:id', async (req: Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id) return generales.manejoErrores('No se envio el id del Empleado', res);
        const empleado = await EmpleadosQueries.getById(id);
        res.status(200).send(empleado);
    } catch (err) {
        next(err);
    }
})

empleadosRoutes.get('/ejecutivos/activos', async (req: Request, res: Response, next) => {
    try {
        const {id} = req.headers;
        if (!id) return generales.manejoErrores('No se nvio el id', res);
        const permisos = await AdministradorPolizasQueries.getPermisosVisualizacion(+id);
        let query = '';
        if (+permisos.idPermisoVisualizacion === 2) {
            // if (+permisos.idPuesto === 7) {
            //     let hijos = await PlazaQueries.getPlazasHijo(permisos.id);
            //     const aux = [];
            //     if (hijos.length === 0){
            //         aux.push(+idEmpleado)
            //     } else {
            //         for (const hijo of hijos) {
            //             aux.push(hijo.id)
            //         }
            //     }
            //     query = `and empleado.id IN (${aux})`;
            // } else {
            const departamentos = await UsuariosDepartamentoQueries.getDepartamentoByIdUsuario(permisos.idUsuario)
            const aux = [];
            if (departamentos.length === 0){
                aux.push(permisos.idDepartamento)
            } else {
                for (const departamento of departamentos) {
                    aux.push(departamento.idDepartamento)
                }
            }
            query = `and departamento.id IN (${aux})`;
            // }
        }
        const empleados = await EmpleadosQueries.getActivos(query)
        res.status(200).send(empleados);
    } catch (err) {
        next(err);
    }
})


export default empleadosRoutes;

