import {Request, Response, Router} from "express";
import AdministradorPolizasQueries from "../../queries/adminPolizas/administradorPolizas-queries";
import UsuariosDepartamentoQueries from "../../queries/usuariosDepartamento/usuariosDepartamentoQueries";

const generales = require('../../general/function');

const UsuariosRoutes = Router();

UsuariosRoutes.get('/:idEmpleado', async (req: Request, res: Response, next) => {
    try {
        const {idEmpleado} = req.params;
        if (!(idEmpleado)) return generales.manejoErrores('No se envio el id empleado', res);
        const permisos = await AdministradorPolizasQueries.getPermisosVisualizacion(+idEmpleado);
        let departamentos;
        switch (permisos.idPermisoVisualizacion) {
            case 1:
                departamentos = await UsuariosDepartamentoQueries.getDepartamentoByIdUsuario(permisos.idUsuario)
                break;
            case 2:
                departamentos = await UsuariosDepartamentoQueries.getDepartamentoByIdUsuario(permisos.idUsuario)
                break;
            case 3:
                departamentos = []
                break;
        }
        res.status(200).send( departamentos );
    } catch (err) {
        next(err);
    }
});
export default UsuariosRoutes;
