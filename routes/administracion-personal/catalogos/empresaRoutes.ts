import {Request, Response, Router} from "express";
import EmpresaService from "../../../queries/administracion-personal/catalogos/empresaService";

const empresaRoutes = Router();

empresaRoutes.get('/getAll', async (req:Request, res:Response, next) => {
    try {
        const empresaData = await EmpresaService.get();
        return res.status(200).send(empresaData);
    } catch (err) {
        next(err);
    }
});

export default empresaRoutes;
