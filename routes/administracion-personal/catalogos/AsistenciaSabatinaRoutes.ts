import {Request, Response, Router} from "express";
import AsistenciaSabatinaService from "../../../queries/administracion-personal/catalogos/AsistenciaSabatina";
const generales = require('../../../general/function');

const AsistenciaSabatinaRoutes = Router();

AsistenciaSabatinaRoutes.get('/getAll', async (req:Request, res: Response, next) => {
    try {
        const AsistenciaSabatinaData = await AsistenciaSabatinaService.get();
        return res.status(200).send(AsistenciaSabatinaData);
    } catch (err) {
        next(err);
    }
});

AsistenciaSabatinaRoutes.post('', async (req: Request, res: Response, next) => {
    try {
        await AsistenciaSabatinaService.create(req.body);
        return res.status(200).send();
    } catch (err) {
        next(err);
    }
});

AsistenciaSabatinaRoutes.put('/:id', async (req: Request, res: Response, next) => {
    try {
        const id = Number(req.params.id);
        if (!id) return  generales.manejoErrores('No se envio el id', res);
        await AsistenciaSabatinaService.update(id, req.body);
        return res.status(200).send();
    } catch (err) {
        next(err);
    }
});


export default AsistenciaSabatinaRoutes;
