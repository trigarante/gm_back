import DriveApiService from "../../../services/drive/driveApiService";
import SolicitudIncapacidadService from "../../../queries/administracion-personal/justificaciones";
const generales = require('../../../general/function');
import {Request, Response, Router} from "express";
import AsistenciasService from "../../../queries/administracion-personal/asistencias/asistenciasService";
import RegistroAsistenciaService from "../../../queries/administracion-personal/asistencias/registroAsistenciaService";
import {generarDiasArray} from "../../../general/asistencias";
import AsistenciasModel from "../../../models/administracion-personal/asistencias/asistenciasModel";
import LogsAdpService from "../../../queries/administracion-personal/logs/logsAdpService";
import {Asistencia} from "../../../interfaces/adp/Asistencia";
import PlazaService from "../../../queries/administracion-personal/PlazaService";
import AdministradorPolizasQueries from "../../../queries/adminPolizas/administradorPolizas-queries";
import UsuariosDepartamentoQueries from "../../../queries/usuariosDepartamento/usuariosDepartamentoQueries";
import PlazaQueries from "../../../queries/RRHH/plazaQueries";
import EmpleadoService from "../../../queries/administracion-personal/empleados";
const router = Router();

router.post('', async (req: Request, res: Response, next) => {
    try {
        console.log(req.body);
        console.log('-------------------------')
        console.log('-------------------------')
        const datos = JSON.parse(req.body.datos);
        const files = req['files'];
        // let cont = 0;
        const asistencia: Asistencia = await AsistenciasService.post(datos);
        if (datos.justificante) {
            const {fechaInicio, fechaFin, idEmpleado, idEmpleadoSupervisor, comentarios, idMotivo} = datos.justificante;
            const idFolder = await DriveApiService.findOrCreateFolder(3, datos.idEmpleado);
            const idFile = await DriveApiService.subirArchivo(idFolder, files.file, 'justificante');
            const justificante = {
                idEmpleado,
                idEmpleadoSolicitante: idEmpleadoSupervisor,
                fechaInicio,
                fechaFin,
                comentarios,
                idMotivoJustificacion: idMotivo,
                documento: idFile
            }
            await SolicitudIncapacidadService.create(justificante);
            // cont++;
        }
        // for (const asistencia of datos) {
        //     await AsistenciasService.post(asistencia);
        //     if (asistencia.justificante) {
        //         const idFolder = await DriveApiService.findOrCreateFolder(3, asistencia.idEmpleado);
        //         const idFile = await DriveApiService.subirArchivo(idFolder, files.file[cont], 'justificante');
        //         await SolicitudIncapacidadService.create({...asistencia.justificante, documento: idFile});
        //         cont++;
        //     }
        // }
        await RegistroAsistenciaService.post({idEmpleado: datos.idEmpleado, idEmpleadoSupervisor: datos.idEmpleadoSupervisor, idAsistencia: asistencia.id})
        res.status(200).send();
    } catch (err) {
        next(err);
    }
});

router.get('/registroAsistenciaByEmpleado', async (req: Request, res: Response, next) => {
    try {
        const {id} = req.headers;
        if (!id) return generales.manejoErrores('No se envio el id', res);
        const data = await RegistroAsistenciaService.getByIdEmpleadoNow(+id);
        console.log('--------------')
        console.log(data)
        res.status(200).send(data);
    } catch (err) {
        next(err);
    }
});
router.get('/faltanHijos', async (req: Request, res: Response, next) => {
    try {
        const idHijos: number[] = [];
        const idRegistrosHoy: number[] = [];
        const idEmpleadosFaltantes: number[] = [];
        const {id} = req.headers;
        if (!id) return generales.manejoErrores('No se envio el id', res);
        const { idPlaza } = await EmpleadoService.getById(id);
        const hijos =  await EmpleadoService.getPlazasHijo(+idPlaza);
        if (hijos) {
            hijos.forEach( hijo => {
                idHijos.push(+hijo.id)
            })
        }
        const registrosDeHoy = await RegistroAsistenciaService.registrosDeHoy(+id);
        if (registrosDeHoy) {
            registrosDeHoy.forEach( registro => {
                idRegistrosHoy.push(+registro.idEmpleado)
            })
        }
        if (idHijos.length > 0){
            idHijos.forEach( empl => {
                if (!idRegistrosHoy.includes(empl)) idEmpleadosFaltantes.push(empl);
            })
        }
        console.log(idHijos)
        console.log(idRegistrosHoy)
        console.log(idEmpleadosFaltantes)
        res.status(200).send(idEmpleadosFaltantes);
    } catch (err) {
        next(err);
    }
});

router.get('/verAsistencias', async (req: Request, res: Response, next) => {
    try {
        const {idempleado, inicio, fin} = req.headers;
        if (!(idempleado && inicio && fin)) return generales.manejoErrores('No se envio la info', res);
        // const permisos = await EmpleadoService.getPermisosVisualizacion(+idempleado);
        // let query = '';
        // switch (+permisos.idPermisoVisualizacion) {
        //     case 2:
        //         let hijos;
        //         if (permisos.idPuesto === 7) {
        //              hijos = await PlazaService.getPlazasHijo(permisos.id);
        //             if (hijos.length === 0){
        //                 hijos = [+idempleado]
        //             }
        //             query = `AND empleado.id IN" = (${hijos})`;
        //         } else {
        //             const departamentos = await UsuariosDepartamentoServices.getDepartamentoByIdUsuario(permisos.idUsuario);
        //             const aux = [];
        //             for (const departamento of departamentos) {
        //                 aux.push(departamento.idDepartamento)
        //             }
        //             query = `AND empleado.id IN" = (${hijos})`;
        //         }
        //         break;
        // }
        const asistencias: any = await AsistenciasService.verAsistencias(inicio, fin);
        const respuesta = [];
        let id = asistencias[0].id;
        let empleado = {
            id,
            nombre: asistencias[0].nombre,
            dias: []
        };
        for(const [i, asistencia] of asistencias.entries()) {
            if (id !== asistencia.id || i === asistencias.length - 1) {
                if (i === asistencias.length - 1) {
                    empleado.dias.push({dia: asistencia.dia, mes: asistencia.mes, estado: asistencia.estadoAsistencia, alias: asistencia.alias});
                }
                respuesta.push(empleado);
                id = asistencia.id
                empleado = {
                    id,
                    nombre: asistencia.nombre,
                    dias: []
                };
            }
            empleado.dias.push({dia: asistencia.dia, mes: asistencia.mes, estado: asistencia.estadoAsistencia, alias: asistencia.alias});
        }
        res.status(200).send(respuesta);
    } catch (err) {
        next(err);
    }
});

router.get('/asistencias/fechas', async (req: Request, res: Response, next) => {
    try {
        const { finicio, ffin, iddepartamento, idempleado } = req.headers;
        // obtener el area
        const idAreadinamico = await PlazaService.getArea(iddepartamento);
        // --P A S O  1 GENERAR ARRAY DÍAS--
        // obtener idMes y año
        const idMesInicio = +finicio.toString().split('-')[1];
        const idAnioInicio = +finicio.toString().split('-')[0];
        const idDiaInicio = +finicio.toString().split('-')[2];
        const idMesFin = +ffin.toString().split('-')[1];
        const idAnioFin = +ffin.toString().split('-')[0];
        const idDiaFin = +ffin.toString().split('-')[2];
        let numMeses: number;
        let diasMes1: number;
        let diasMes2: number;
        let diasMes3: number;
        let dias1 = [];
        let dias1Temp = [];
        let dias2 = [];
        let dias2Temp = [];
        let dias3 = [];
        let dias3Temp = [];
        let diasFinalTemp = [];
        let diasFinal = [];
        let mesTemp: number;
        const diaInicioTemp = idDiaInicio - 1;;
        // verificar si es un mes, dos o tres
        if (idMesInicio === idMesFin )
            numMeses = 1;
        else if (idMesFin - idMesInicio === 1 || idMesFin - idMesInicio === -11  )
            numMeses = 2;
        else if (idMesFin - idMesInicio === 2 || idMesFin - idMesInicio === -10 )
            numMeses = 3;
        // generar array de dias
        switch (numMeses) {
            case 1:
                diasMes1 = new Date(idAnioFin, idMesFin, 0).getDate();
                dias1 = generarDiasArray(diasMes1, idMesInicio, idAnioInicio);
                // recortar de acuerdo a la fecha
                diasFinal = dias1.slice(diaInicioTemp, idDiaFin)
                break;
            case 2:
                diasMes1 = new Date(idAnioInicio, idMesInicio, 0).getDate();
                diasMes2 = new Date(idAnioFin, idMesFin, 0).getDate();
                dias1 = generarDiasArray(diasMes1, idMesInicio, idAnioInicio);
                dias2 = generarDiasArray(diasMes2, idMesFin, idAnioFin);
                // recortar de acuerdo a la fecha
                dias1Temp = dias1.slice(diaInicioTemp, diasMes1);
                dias2Temp = dias2.slice(0, idDiaFin);
                // unir en un array dias final
                diasFinal = dias1Temp.concat(dias2Temp);
                break;
            case 3:
                if (idMesFin - idMesInicio === 2) {
                    mesTemp = idMesInicio + 1;
                    diasMes1 = new Date(idAnioInicio, idMesInicio, 0).getDate();
                    diasMes2 = new Date(idAnioInicio, mesTemp , 0).getDate();
                    diasMes3 = new Date(idAnioFin, idMesFin, 0).getDate();
                    dias1 = generarDiasArray(diasMes1, idMesInicio, idAnioInicio);
                    dias2 = generarDiasArray(diasMes2, mesTemp, idAnioInicio);
                    dias3 = generarDiasArray(diasMes3, idMesFin, idAnioFin);
                } else if (idMesFin - idMesInicio === -10) {
                    diasMes1 = new Date(idAnioInicio, idMesInicio, 0).getDate();
                    if ( idMesInicio === 12 ) {
                        mesTemp = 1;
                        diasMes2 = new Date(idAnioFin, mesTemp , 0).getDate();
                        dias2 = generarDiasArray(diasMes2, mesTemp, idAnioFin);
                    } else if ( idMesInicio === 11 ) {
                        mesTemp = 12;
                        diasMes2 = new Date(idAnioInicio, mesTemp , 0).getDate();
                        dias2 = generarDiasArray(diasMes2, mesTemp, idAnioInicio);
                    }
                    diasMes3 = new Date(idAnioFin, idMesFin, 0).getDate();
                    dias1 = generarDiasArray(diasMes1, idMesInicio, idAnioInicio);
                    dias3 = generarDiasArray(diasMes3, idMesFin, idAnioFin);
                }
                // recortar de acuerdo a la fecha
                dias1Temp = dias1.slice(diaInicioTemp, diasMes1);
                dias3Temp = dias3.slice(0, idDiaFin);
                // unir en un array dias final
                diasFinalTemp = dias1Temp.concat(dias2);
                diasFinal = diasFinalTemp.concat(dias3Temp);
                break;
            default:
                return res.status(400).json({msg: 'Se exedió el límite'});
        }
        // --P A S O  2 GENERAR ARRAY CON DATOS Y ASISTENCIAS--
        // obtener empleados y sus asistencias
        let queryDepartamento;
        const queryArea = `AND d."idArea" = ${+idAreadinamico.idArea}`
        const permisos = await AdministradorPolizasQueries.getPermisosVisualizacion(+idempleado);
        console.log(permisos)
        let datos;
        if (permisos.idPermisoVisualizacion === 1) {
            queryDepartamento = +iddepartamento === 0 ? '' : `AND d.id = ${+iddepartamento}`
            datos = await AsistenciasService.obtenerEmpleados(queryDepartamento, queryArea);
        } else if(permisos.idPermisoVisualizacion === 2){
            const aux: any[] = [];
            // if (permisos.idPuesto === 7) {
                const hijos = await PlazaQueries.getPlazasHijo(permisos.id);
                for (const hijo of hijos) {
                    aux.push(hijo.id)
                }
                // queryDepartamento = +iddepartamento === 0 ? '' : `AND d.id IN (${aux})`;
                queryDepartamento =  `AND e.id IN (${aux})`;
                // queryArea = '';
                datos = aux.length === 0 ? [] : await AsistenciasService.obtenerEmpleados(queryDepartamento, queryArea);
            // } else {
            //     const departamentos = await UsuariosDepartamentoQueries.getDepartamentoByIdUsuario(permisos.idUsuario)
            //     for (const departamento of departamentos) {
            //         aux.push(departamento.idDepartamento)
            //     }
            //     if (departamentos.length === 0){
            //         aux.push(permisos.idDepartamento)
            //     }
            //     console.log(aux)
            //     queryDepartamento = +iddepartamento === 0 ? '' : `AND d.id IN (${aux})`;
            //     datos = await AsistenciasService.obtenerEmpleados(queryDepartamento, queryArea);
            // }
        }
        const asistenciasEmpleados: any = await AsistenciasService.verAsistencias(finicio, ffin);
        for (let i = 0; i < datos.length; i++) {
            // generar un arreglo del empleado con sus asistencias
            const asistenciasResult = asistenciasEmpleados.filter(asistencia => asistencia.id === datos[i].id);
            const asistencias: any[] = [];
            const diasConAsistencia: any[] = [];
            const diasNum: number[] = [];
            const fechasNum: number[] = [];
            const fechasAsistidas: any[] = []
            const fechasNoAsistidas: any[] = []
            const fechasArray: any[] = []
            for (let o = 0; o < diasFinal.length; o++) {
                let nuevostring;
                // generar un arreglo con las asistencias que tuvo ese empleado en el mes
                if (asistenciasResult[o] !== undefined){
                    const nuevoJson = JSON.stringify(asistenciasResult[o]);
                    diasConAsistencia.push(+asistenciasResult[o].dia); // array para ver los dias y su num que asistio cada empleado
                    fechasAsistidas.push(asistenciasResult[o].fecha)
                    nuevostring = nuevoJson.replace('alias', `${asistenciasResult[o].fecha}`);
                    asistencias.push(JSON.parse(nuevostring));
                }
                // obtener los dias y su numero en un array
                diasNum.push(diasFinal[o].num);
                // obtener un array con las fechas
                fechasNum.push(diasFinal[o].fecha)
                fechasArray.push(asistenciasResult[o] === undefined ? null : asistenciasResult[o].fecha)
            }
            // obtener los anios que no tuvo asistencia para insertar json nulo
            if(fechasAsistidas.length > 0) {
                fechasNum.forEach(dia => {
                    if (!fechasAsistidas.includes(dia))
                        fechasNoAsistidas.push(dia);
                })
                for (let j = 0; j < fechasNoAsistidas.length; j++) {
                    const anio = +fechasNoAsistidas[j].toString().split('-')[0];
                    const mes = +fechasNoAsistidas[j].toString().split('-')[1];
                    const dia = +fechasNoAsistidas[j].toString().split('-')[2];
                    const strinNull = `{"id":null,"nombre":null,"anio":${anio},"fecha":null,"dia":${dia},"mes":${mes},"solicitud":false,"diaSemana":null,"nombreDia":null,"conteo":null,"estadoAsistencia":null,"${fechasNoAsistidas[j]}": null,"idAsistencia":null}`;
                    asistencias.splice(j,0,JSON.parse(strinNull))
                }
            } else {
                for (let j = 0; j < fechasNum.length; j++) {
                    const anio = +fechasNum[j].toString().split('-')[0];
                    const mes = +fechasNum[j].toString().split('-')[1];
                    const dia = +fechasNum[j].toString().split('-')[2];
                    const strinNull = `{"id":null,"nombre":null,"anio":${anio},"fecha":null,"dia":${dia},"mes":${mes},"solicitud":false,"diaSemana":null,"nombreDia":null,"conteo":null,"estadoAsistencia":null,"${fechasNum[j]}": null,"idAsistencia":null}`;
                    asistencias.push(JSON.parse(strinNull))
                }

            }
            // ordenar por dia
            for (let k = 1; k < asistencias.length; k++) {
                // Escoger el primer elemento del array desordenado
                const currentDia = +asistencias[k].dia;
                const current = asistencias[k];
                // El ultimo elemento del array ordenado
                let w = k-1;
                while ((w > -1) && (currentDia < +asistencias[w].dia)) {
                    asistencias[w+1] = asistencias[w];
                    w--;
                }
                asistencias[w+1] = current;
            }
            // ordenar por mes
            for (let k = 1; k < asistencias.length; k++) {
                // Escoger el primer elemento del array desordenado
                const currentDia = +asistencias[k].mes;
                const current = asistencias[k];
                // El ultimo elemento del array ordenado
                let w = k-1;
                while ((w > -1) && (currentDia < +asistencias[w].mes)) {
                    asistencias[w+1] = asistencias[w];
                    w--;
                }
                asistencias[w+1] = current;
            }
            // ordenar por año
            for (let k = 1; k < asistencias.length; k++) {
                // Escoger el primer elemento del array desordenado
                const currentDia = +asistencias[k].anio;
                const current = asistencias[k];
                // El ultimo elemento del array ordenado
                let w = k-1;
                while ((w > -1) && (currentDia < +asistencias[w].anio)) {
                    asistencias[w+1] = asistencias[w];
                    w--;
                }
                asistencias[w+1] = current;
            }
            // generar datos de salida
            datos[i] = {
                ...datos[i],
                asistencias
            }
        }
        const aux = {
            diasFinal,
            datos
        };
        return res.status(200).send(aux)
    } catch (err) {
        console.log(err)
        next(err);
    }
});

router.put('/actualizar', async (req: Request, res: Response, next) => {
    try {
        console.log(req.body)
        const logs = await AsistenciasModel.findByPk(req.body.id);
        const plaza = await AsistenciasService.obtenerEmpleadoPlaza(+req.body.idEmpleadoEditor);
        const aux = {idEmpleadoModificante: +req.body.idEmpleadoEditor, idEmpleado: req.body.idEmpleadoAEditar, idPlaza: plaza.id, idTipoModificacionAdp: 11, json: logs};
        await LogsAdpService.create(aux)
        const aux2={
            id: req.body.id,
            idEmpleado: req.body.idEmpleadoAEditar,
            idEstadoAsistencia: req.body.idEstadoAsistencia
        }
        const result = await AsistenciasService.update(req.body.id, aux2);
        res.status(200).send(result);
    } catch (err) {
        next(err);
    }
});
router.get('/lambda', async (req: Request, res: Response, next) => {
    try{
        const siAsistenciaArray: {id?: number}[] | void = await AsistenciasService.siAsistieron().catch(e => console.log(e));
        const idConAsistencia: number[] = [];
        const idNoAsistencia: number[] = [];
        let padresAusentes: number = 0;
        console.log('siAsistenciaArray')
        console.log(siAsistenciaArray)
        if(siAsistenciaArray !== [] && siAsistenciaArray){
            siAsistenciaArray.forEach(id => idConAsistencia.push(id.id))
            if (idConAsistencia.length === 0) idConAsistencia.push(0)
            const noAsistieronArray: {id?: number}[] | void = await AsistenciasService.noAsistieron(idConAsistencia).catch(e => console.log(e));
            console.log('noAsistieronArray')
            console.log(noAsistieronArray)
            if (noAsistieronArray !== [] && noAsistieronArray ){
                noAsistieronArray.forEach(id => idNoAsistencia.push(id.id))
            } else {
                res.status(200).json({msg: 'Todos los empleados tienen asistencia'})
            }
            console.log('idNoAsistencia')
            console.log(idNoAsistencia)
            for await (const id of idNoAsistencia) {
                const idPadre: {idPadre?: number}| void = await PlazaService.getIdPadre(id).catch(e => console.log(e));
                if (idPadre){
                    const asistencia: Asistencia = {
                        idEmpleado: id,
                        idEmpleadoSupervisor: idPadre.idPadre,
                        idEstadoAsistencia: 15,
                    }
                    await AsistenciasService.post(asistencia).catch(e => console.log(e));
                } else {
                    console.log('sinPapa');
                    padresAusentes++;
                    const asistencia: Asistencia = {
                        idEmpleado: id,
                        idEstadoAsistencia: 15,
                    }
                    await AsistenciasService.post(asistencia).catch(e => console.log(e));
                }
            }
            res.status(200).json({msg:`Se crearon las asistencias con ${padresAusentes} padres ausentes`})
        } else {
            res.status(500).json({msg: 'Ningún empleado tuvo asistencia, algo podría estar mal'})
        }
        // console.log(idConAsistencia)
    } catch (err){
        next(err);
    }
});
export default router;
