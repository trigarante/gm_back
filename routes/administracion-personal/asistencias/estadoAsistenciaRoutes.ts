import {Request, Response, Router} from "express";
import EstadoAsistenciaService from "../../../queries/administracion-personal/asistencias/estadoAsistenciaService";

const router = Router();

router.get('', async (req: Request, res: Response, next) => {
    try {
        const datos = await EstadoAsistenciaService.getActivos();
        res.status(200).send(datos);
    } catch (e) {
        next(e)
    }
})

export default router;
