import {Request, Response, Router} from "express";
import DriveApiService from "../../../services/drive/driveApiService";
import AsistenciasService from "../../../queries/administracion-personal/asistencias/asistenciasService";
import {UltimoMovimiento} from "../../../interfaces/adp/ultimoMovimiento";
import UltimosMovimientosService from "../../../queries/administracion-personal/asistencias/ultimosMovimientosService";
import {generarDiasArray} from "../../../general/asistencias";
import router from "./asistenciasRoutes";
import EmpleadoService from "../../../queries/administracion-personal/empleados";
import PlazaQueries from "../../../queries/RRHH/plazaQueries";
import UsuariosDepartamentoServices from "../../../queries/ti/usuariosDepartamentoServices";
import SolicitudVacacionesService from "../../../queries/administracion-personal/vacaciones/solicitudVacaciones";
import SolicitudVacacionesRoutes from "../vacaciones/solicitudVacaciones";
import {Asistencia} from "../../../interfaces/adp/Asistencia";
import EstadoAsistenciaService from "../../../queries/administracion-personal/asistencias/estadoAsistenciaService";

const generales = require('../../../general/function');

const ultimosMovimientos = Router();

ultimosMovimientos.post('/', async (req:Request, res: Response, next) => {
    try {
        const datos = JSON.parse(req.body.datos);
        console.log(datos)
        const {idEmpleado, idEmpleadoSupervisor, idMotivo,
                idAsistencia, fechaFin, fechaInicio, idEstadoAsistencia, idCierre, comentarios} = datos
        const files = req['files'].file;
        console.log(files)
        const idFolder = await DriveApiService.findOrCreateFolder(3, idEmpleado);
        const idFile: string | void = await DriveApiService.subirArchivo(idFolder, files, 'ultimoMovimiento');
        await AsistenciasService.update(idAsistencia, {solicitud: true});
        const ultimoMovimiento: UltimoMovimiento = {
            idEmpleado,
            idEmpleadoSupervisor,
            idMotivo,
            documento: idFile,
            comentarios,
            fechaFin,
            fechaInicio,
            idEstadoAsistencia,
            idAsistencia,
            idCierre,
        }
        const um = await UltimosMovimientosService.postMovimiento(ultimoMovimiento);
        return res.status(200).send(um);
    } catch (err) {
        next(err);
    }
});
ultimosMovimientos.get('/unoEnProceso/:idAsistencia', async (req:Request, res: Response, next) => {
    try {
        const {idAsistencia} = req.params;
        const tieneSolicitudEnProceso = await AsistenciasService.encontrarDuplicadoUltimoMovi(+idAsistencia);
        return res.status(200).send(!!tieneSolicitudEnProceso)
    } catch (err) {
        next(err);
    }
});
ultimosMovimientos.get('/byEmpleadoSupervisor', async (req: Request, res: Response, next) => {
    try {
        const {id, idestado} = req.headers;
        console.log(id)
        if (!(id && idestado)) return generales.manejoErrores('No se envio el id', res);
        const cantidadVacaciones: any = await UltimosMovimientosService.byEmpleadoSupervisor(+id , +idestado);
        console.log(cantidadVacaciones)
        // OBTENER EL ESTADO ANTERIOR
        const idAsistenciaArray: number[] = [];
        const idEstadosAnterioresArray: number[] = [];
        const estadosAnterioresArray: string[] = [];
        cantidadVacaciones.forEach( vacacion => idAsistenciaArray.push(vacacion.idAsistencia));
        for  (const idAsis of idAsistenciaArray) {
            const asistencia: any = await AsistenciasService.traerAsistencia(idAsis);
            if (asistencia) idEstadosAnterioresArray.push(asistencia.idEstadoAsistencia)
        }
        if (idEstadosAnterioresArray.length > 0){
            for  (const idEdo of idEstadosAnterioresArray) {
                const estado: any = await EstadoAsistenciaService.getDescripcionByid(idEdo);
                if (estado) estadosAnterioresArray.push(estado.descripcion)
            }
            for (const vacacion of cantidadVacaciones) {
                estadosAnterioresArray.forEach( edo => vacacion.estadoAnterior = edo)
            }
        }
        console.log(cantidadVacaciones)

        return res.status(200).send(cantidadVacaciones);
    } catch (err) {
        next(err);
    }
});
ultimosMovimientos.get('/ultimosMovimientos/fechas/:esUltimo', async (req: Request, res: Response, next) => {
    try {
        const esUltimo = req.params.esUltimo;
        // --P A S O  1 GENERAR ARRAY DÍAS--
        // obtener idMes y año
        const { finicio, ffin, iddepartamento, idarea } = req.headers;
        console.log(finicio);
        console.log(ffin);
        console.log(iddepartamento);
        console.log(idarea);

        const idMesInicio = +finicio.toString().split('-')[1];
        const idAnioInicio = +finicio.toString().split('-')[0];
        const idDiaInicio = +finicio.toString().split('-')[2];
        const idMesFin = +ffin.toString().split('-')[1];
        const idAnioFin = +ffin.toString().split('-')[0];
        const idDiaFin = +ffin.toString().split('-')[2];
        let numMeses: number;
        let diasMes1: number;
        let diasMes2: number;
        let diasMes3: number;
        let dias1 = [];
        let dias1Temp = [];
        let dias2 = [];
        let dias2Temp = [];
        let dias3 = [];
        let dias3Temp = [];
        let diasFinalTemp = [];
        let diasFinal = [];
        let mesTemp: number;
        const diaInicioTemp = idDiaInicio - 1;;
        // verificar si es un mes, dos o tres
        if (idMesInicio === idMesFin )
            numMeses = 1;
        else if (idMesFin - idMesInicio === 1 || idMesFin - idMesInicio === -11  )
            numMeses = 2;
        else if (idMesFin - idMesInicio === 2 || idMesFin - idMesInicio === -10 )
            numMeses = 3;
        // generar array de dias
        switch (numMeses) {
            case 1:
                diasMes1 = new Date(idAnioFin, idMesFin, 0).getDate();
                dias1 = generarDiasArray(diasMes1, idMesInicio, idAnioInicio);
                // recortar de acuerdo a la fecha
                diasFinal = dias1.slice(diaInicioTemp, idDiaFin)
                break;
            case 2:
                diasMes1 = new Date(idAnioInicio, idMesInicio, 0).getDate();
                diasMes2 = new Date(idAnioFin, idMesFin, 0).getDate();
                dias1 = generarDiasArray(diasMes1, idMesInicio, idAnioInicio);
                dias2 = generarDiasArray(diasMes2, idMesFin, idAnioFin);
                // recortar de acuerdo a la fecha
                dias1Temp = dias1.slice(diaInicioTemp, diasMes1);
                dias2Temp = dias2.slice(0, idDiaFin);
                // unir en un array dias final
                diasFinal = dias1Temp.concat(dias2Temp);
                break;
            case 3:
                if (idMesFin - idMesInicio === 2) {
                    mesTemp = idMesInicio + 1;
                    diasMes1 = new Date(idAnioInicio, idMesInicio, 0).getDate();
                    diasMes2 = new Date(idAnioInicio, mesTemp , 0).getDate();
                    diasMes3 = new Date(idAnioFin, idMesFin, 0).getDate();
                    dias1 = generarDiasArray(diasMes1, idMesInicio, idAnioInicio);
                    dias2 = generarDiasArray(diasMes2, mesTemp, idAnioInicio);
                    dias3 = generarDiasArray(diasMes3, idMesFin, idAnioFin);
                } else if (idMesFin - idMesInicio === -10) {
                    diasMes1 = new Date(idAnioInicio, idMesInicio, 0).getDate();
                    if ( idMesInicio === 12 ) {
                        mesTemp = 1;
                        diasMes2 = new Date(idAnioFin, mesTemp , 0).getDate();
                        dias2 = generarDiasArray(diasMes2, mesTemp, idAnioFin);
                    } else if ( idMesInicio === 11 ) {
                        mesTemp = 12;
                        diasMes2 = new Date(idAnioInicio, mesTemp , 0).getDate();
                        dias2 = generarDiasArray(diasMes2, mesTemp, idAnioInicio);
                    }
                    diasMes3 = new Date(idAnioFin, idMesFin, 0).getDate();
                    dias1 = generarDiasArray(diasMes1, idMesInicio, idAnioInicio);
                    dias3 = generarDiasArray(diasMes3, idMesFin, idAnioFin);
                }
                // recortar de acuerdo a la fecha
                dias1Temp = dias1.slice(diaInicioTemp, diasMes1);
                dias3Temp = dias3.slice(0, idDiaFin);
                // unir en un array dias final
                diasFinalTemp = dias1Temp.concat(dias2);
                diasFinal = diasFinalTemp.concat(dias3Temp);
                break;
            default:
                return res.status(400).json({msg: 'Se exedió el límite'});
        }
        // --P A S O  2 GENERAR ARRAY CON DATOS Y ASISTENCIAS--
        // obtener empleados y sus asistencias
        const queryDepartamento = +iddepartamento === 0 ? '' : `AND d.id = ${+iddepartamento}`
        const queryArea = +idarea === 0 ? '' : `AND d."idArea" = ${+idarea}`
        let datos = await AsistenciasService.obtenerEmpleados(queryDepartamento, queryArea);
        let asistenciasEmpleados: any;
        if (+esUltimo === 1) {
            const asit: any = await AsistenciasService.verAsistencias(finicio, ffin);
            asistenciasEmpleados = asit.filter(asistencia => asistencia.solicitud === true);
        } else if(+esUltimo === 2){
            asistenciasEmpleados= await AsistenciasService.verAsistencias(finicio, ffin);
        }
        for (let i = 0; i < datos.length; i++) {
            // generar un arreglo del empleado con sus asistencias
            const asistenciasResult = asistenciasEmpleados.filter(asistencia => asistencia.id === datos[i].id);
            const asistencias: any[] = [];
            const diasConAsistencia: any[] = [];
            const diasNum: number[] = [];
            const fechasNum: number[] = [];
            const fechasAsistidas: any[] = []
            const fechasNoAsistidas: any[] = []
            const fechasArray: any[] = []
            for (let o = 0; o < diasFinal.length; o++) {
                let nuevostring;
                // generar un arreglo con las asistencias que tuvo ese empleado en el mes
                if (asistenciasResult[o] !== undefined){
                    const nuevoJson = JSON.stringify(asistenciasResult[o]);
                    diasConAsistencia.push(+asistenciasResult[o].dia); // array para ver los dias y su num que asistio cada empleado
                    fechasAsistidas.push(asistenciasResult[o].fecha)
                    nuevostring = nuevoJson.replace('alias', `${asistenciasResult[o].fecha}`);
                    asistencias.push(JSON.parse(nuevostring));
                }
                // obtener los dias y su numero en un array
                diasNum.push(diasFinal[o].num);
                // obtener un array con las fechas
                fechasNum.push(diasFinal[o].fecha)
                fechasArray.push(asistenciasResult[o] === undefined ? null : asistenciasResult[o].fecha)
            }
            // obtener los anios que no tuvo asistencia para insertar json nulo
            if(fechasAsistidas.length > 0) {
                fechasNum.forEach(dia => {
                    if (!fechasAsistidas.includes(dia))
                        fechasNoAsistidas.push(dia);
                })
                for (let j = 0; j < fechasNoAsistidas.length; j++) {
                    const anio = +fechasNoAsistidas[j].toString().split('-')[0];
                    const mes = +fechasNoAsistidas[j].toString().split('-')[1];
                    const dia = +fechasNoAsistidas[j].toString().split('-')[2];
                    const strinNull = `{"id":null,"nombre":null,"anio":${anio},"fecha":null,"dia":${dia},"mes":${mes},"solicitud":false,"diaSemana":null,"nombreDia":null,"conteo":null,"estadoAsistencia":null,"${fechasNoAsistidas[j]}": null,"idAsistencia":null}`;
                    asistencias.splice(j,0,JSON.parse(strinNull))
                }
            } else {
                for (let j = 0; j < fechasNum.length; j++) {
                    const anio = +fechasNum[j].toString().split('-')[0];
                    const mes = +fechasNum[j].toString().split('-')[1];
                    const dia = +fechasNum[j].toString().split('-')[2];
                    const strinNull = `{"id":null,"nombre":null,"anio":${anio},"fecha":null,"dia":${dia},"mes":${mes},"solicitud":false,"diaSemana":null,"nombreDia":null,"conteo":null,"estadoAsistencia":null,"${fechasNum[j]}": null,"idAsistencia":null}`;
                    asistencias.push(JSON.parse(strinNull))
                }

            }
            // ordenar por dia
            for (let k = 1; k < asistencias.length; k++) {
                // Escoger el primer elemento del array desordenado
                const currentDia = +asistencias[k].dia;
                const current = asistencias[k];
                // El ultimo elemento del array ordenado
                let w = k-1;
                while ((w > -1) && (currentDia < +asistencias[w].dia)) {
                    asistencias[w+1] = asistencias[w];
                    w--;
                }
                asistencias[w+1] = current;
            }
            // ordenar por mes
            for (let k = 1; k < asistencias.length; k++) {
                // Escoger el primer elemento del array desordenado
                const currentDia = +asistencias[k].mes;
                const current = asistencias[k];
                // El ultimo elemento del array ordenado
                let w = k-1;
                while ((w > -1) && (currentDia < +asistencias[w].mes)) {
                    asistencias[w+1] = asistencias[w];
                    w--;
                }
                asistencias[w+1] = current;
            }
            // ordenar por año
            for (let k = 1; k < asistencias.length; k++) {
                // Escoger el primer elemento del array desordenado
                const currentDia = +asistencias[k].anio;
                const current = asistencias[k];
                // El ultimo elemento del array ordenado
                let w = k-1;
                while ((w > -1) && (currentDia < +asistencias[w].anio)) {
                    asistencias[w+1] = asistencias[w];
                    w--;
                }
                asistencias[w+1] = current;
            }
            // generar datos de salida
            datos[i] = {
                ...datos[i],
                asistencias
            }
        }
        const aux = {
            diasFinal,
            datos
        };
        return res.status(200).send(aux)
    } catch (err) {
        console.log(err)
        next(err);
    }
});
ultimosMovimientos.get('/obtenerArchivo', async (req: Request, res: Response, next) => {
    try {
        const id = req.headers.id;
        if (!id) return generales.manejoErrores('No se envio el id', res);
        const archivo = await DriveApiService.obtenerArchivo(id);
        return res.status(200).send([archivo]);
    } catch (err) {
        next(err);
    }
});
export default ultimosMovimientos;
