import {Router, Request, Response} from "express";
import EstadoInspeccionQueries from "../../queries/estadoInspeccion/estadoInspeccionQueries";

const estadoInspeccionRoutes = Router();

estadoInspeccionRoutes.get('', async(req:Request, res: Response, next) => {
    try {
        const inspeccion = await EstadoInspeccionQueries.findAll();
        res.status(200).send( inspeccion );
    } catch (e) {
        res.status(500).send(e)
    }
})


export default estadoInspeccionRoutes;
