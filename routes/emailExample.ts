import emailConfig from "../emailConfig";
import {Response} from "express";
const generales = require('../general/function');

class EmailExample {

    static async emailSend (email, subject, text, res: Response) {
        try {
            await emailConfig.transporter.sendMail({
                from: '"No Reply" <no-reply@goagentesdigitales.com>',
                to: email,
                subject,
                text,
                html: ``,
                headers: {'default-encoding': 'UTF-8'}
            });
        } catch (e) {
            return generales.manejoErrores(e, res, 500);
        }
    }
}

export default EmailExample;
