import {Request, Response, Router} from "express";
import MotivosDesactivacionQueries from "../../queries/activacionEmpleado/motivosDesactivacionQueries";

const MotivosDesactivacionRouter = Router();

MotivosDesactivacionRouter.get('', async (req: Request, res: Response, next) => {
   try {
       const {tipo} = req.headers;
       const motivos = await MotivosDesactivacionQueries.getActivosByIdTipo(+tipo);
       res.send(motivos);
   } catch (e) {
       next(e)
   }
});

export default MotivosDesactivacionRouter
