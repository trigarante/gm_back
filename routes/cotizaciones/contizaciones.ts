import {Request, Response, Router} from "express";
import CotizacionesQueries from "../../queries/cotizador/cotizacionesQueries";
const cotizacionesRoutes = Router();

cotizacionesRoutes.post('', async (req: Request, res: Response, next) => {
    try {
        // req.body
        const cotizacion = await CotizacionesQueries.postGM(req.body);
        res.status(200).send(cotizacion);
    } catch (err) {
        next(err);
    }
})


export default cotizacionesRoutes;

