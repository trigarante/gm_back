import {Request, Response, Router} from "express";
import ProductividadQueries from "../../queries/productividad/productividadQueries";

const generales = require('../../general/function');
const ProductividadRoutes = Router();

ProductividadRoutes.get('/:idDepartamento/:idTipoContacto/:fechaInicio/:fechaFin', async (req: Request, res: Response, next) => {
    try {
        const {idDepartamento, fechaInicio, fechaFin} = req.params
        if (!fechaInicio ) return generales.manejoErrores('No se envio el id Registro', res);
        let produtividad;
        if(idDepartamento === '0') produtividad = await ProductividadQueries.productividad(fechaInicio,fechaFin)
        else produtividad = await ProductividadQueries.productividadDepartamento(idDepartamento,fechaInicio,fechaFin)
        res.status(200).send( produtividad );
    } catch (err) {
        next(err);
    }
});

export default ProductividadRoutes;
