import {Router, Request, Response} from "express";
import TipoEndosoQueries from "../../queries/tipoEndoso/TipoEndosoQueries";
const generales = require('../../general/function');

const tipoEndosoRoutes = Router();

tipoEndosoRoutes.get('/', async(req:Request, res: Response, next) => {
    try {
        const tipoEndoso = await TipoEndosoQueries.getAll();
        res.status(200).send( tipoEndoso );
    } catch (e) {
        res.sendStatus(500);
    }
})


export default tipoEndosoRoutes;
