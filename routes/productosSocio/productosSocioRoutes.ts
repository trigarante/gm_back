import {Request, Response, Router} from "express";
import generales from "../../general/function";
import ProductosSocioQueries from "../../queries/productosSocio/productosSocioQueries";

const router = Router();

router.get('/getByIdSubRamo', async (req: Request, res: Response, next) => {
    try {
        const {id} = req.headers;
        if (!id) return generales.manejoErrores('No se envio ningun id', res);
        const ramos = await ProductosSocioQueries.getByIdSubRamo(id);
        res.status(200).send(ramos);
    } catch (err) {
        next(err);
    }
})

export default router;
